import numpy as np
import pandas as pd
import scipy.stats as st

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class AverageByTrialMeasurement(Metric):
    """This is a universal metric for calculating the average of the trials' measure."""

    metric_name = 'average_by_trial_measurement'
    required_columns = (
        ROUND_COLUMN_NAME,
        'measure',
    )

    def __init__(self, metric_name, **kwargs) -> None:
        AverageByTrialMeasurement.metric_name = metric_name
        super().__init__(**kwargs)

    @staticmethod
    def calculate(trials):
        random_regrets = []
        metric_name = AverageByTrialMeasurement.metric_name
        for trial_df in trials:
            trial_df = trial_df.sort_values('round')
            trial_df[metric_name] = trial_df['measure']

            random_regrets.append(
                pd.DataFrame(
                    trial_df[[ROUND_COLUMN_NAME, metric_name]]
                )
            )

        def f(row):
            v = row[metric_name]

            if len(v) <= 1:
                return None, None

            scale = st.sem(v)
            if scale == 0:
                return None, None

            return st.t.interval(0.95, len(v)-1, loc=np.mean(v), scale=st.sem(v))

        df = pd.concat(random_regrets).groupby('round').aggregate(lambda x: tuple(x))
        df[f'{metric_name}_confidence_interval'] = df.apply(f, axis=1)
        del df[metric_name]

        return [
            pd.merge(
                pd.concat(random_regrets).groupby('round').mean(),
                df,
                left_on='round',
                right_on='round',
                how='inner',
            )
        ]


if __name__ == '__main__':
    regret = AverageByTrialMeasurement(
        metric_name='loop_amp',
        data_path=(
            './results/closed-loop-feedback-experiment/49/additive_noise/trial-num:*/data-*.parquet'
        ),
        result_path=(
            './results/closed-loop-feedback-experiment/49/additive_noise/'
        ),
        columns_mapping={
            'measure': lambda row: np.linalg.norm(row['parametrization'] - row['init_parametrization'])**2,
        },
    )

    import time
    start = time.time()
    regret()
    end = time.time()
    print('Execution time: ', end - start)
