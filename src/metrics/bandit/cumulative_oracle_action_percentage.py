# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'cumulative_oracle_action_percentage'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class CumulativeOracleActionPercentage(Metric):
    """Proportion of the choice of optimal actions in each round.

    Cumulative metric (takes into account all past rounds).
    Averaging is performed over the trials.
    """

    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'selected_action_indexes',
        'all_action_rewards',
    )

    @staticmethod
    def calculate(trials):
        random_regrets = []
        for trial_df in trials:
            is_round_zero_based = False
            if trial_df[ROUND_COLUMN_NAME].min() == 0:
                is_round_zero_based = True

            def f(row):
                actions_reward = row['all_action_rewards'][row['selected_action_indexes']].sum()
                largest_reward = np.sort(row['all_action_rewards'])[-len(row['selected_action_indexes']):].sum()
                return int(actions_reward == largest_reward)

            trial_df['oracle_action_probability'] = trial_df.apply(f, axis=1)
            trial_df['cumulative_oracle_action_probability'] = trial_df['oracle_action_probability'].cumsum()

            def f(row):
                current_round = row[ROUND_COLUMN_NAME] + is_round_zero_based
                return row['cumulative_oracle_action_probability'] / current_round

            trial_df['percentage_per_trial'] = trial_df.apply(f, axis=1)
            random_regrets.append(
                pd.DataFrame(trial_df[[ROUND_COLUMN_NAME, 'percentage_per_trial']])
            )
        result_df = pd.concat(random_regrets).groupby(ROUND_COLUMN_NAME).sum()
        result_df[METRIC_COLUMN_NAME] = (
            result_df['percentage_per_trial'].div(len(random_regrets))
        )
        del result_df['percentage_per_trial']
        return [result_df]


if __name__ == '__main__':
    regret = CumulativeOracleActionPercentage(
        data_path=(
            './results/simple-experiment/49/data-0.parquet'
        ),
        result_path=(
            './results/simple-experiment/49/'
        ),
        columns_mapping={
            'selected_action_indexes': lambda row: row['actions'],
            'all_action_rewards': lambda row: row['rewards'],
        },
    )

    import time
    start = time.time()
    regret()
    end = time.time()
    print('Execution time: ', end - start)
