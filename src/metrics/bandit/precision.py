# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'precision'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class Precision(Metric):
    """The cumulative precision of choosing the actions on each round.

    Averaging is performed over the trials and users.

    [1] Wang L. et al. Biucb: A contextual bandit algorithm for cold-start and diversified recommendation //2017 IEEE
        International Conference on Big Knowledge (ICBK). – IEEE, 2017. – С. 248-253.
    """
    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'actions',
        'user_preference_keys',
        'user_preference_values'
    )

    @staticmethod
    def calculate(trials):
        trial_precisions = []
        for trial_df in trials:
            def f(row):
                actions = row['actions']
                precisions = []
                for user, preference in zip(row['user_preference_keys'], row['user_preference_values']):
                    s_actions = set(actions)
                    precision = len(s_actions.intersection(set(preference))) / len(s_actions)
                    precisions.append(precision)
                return np.mean(precisions)

            trial_df = trial_df.sort_values('round')
            trial_df[METRIC_COLUMN_NAME] = trial_df.apply(f, axis=1)
            trial_df[METRIC_COLUMN_NAME] = trial_df['precision'].cumsum() / (trial_df['round'] + 1)

            trial_precisions.append(
                pd.DataFrame(trial_df[[ROUND_COLUMN_NAME, METRIC_COLUMN_NAME]])
            )
        return [
            pd.concat(trial_precisions).groupby('round').mean(),
        ]
