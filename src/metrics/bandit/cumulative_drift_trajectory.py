# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd
from collections import Counter

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'drift_divergence'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class CumulativeDriftTrajectory(Metric):
    """
    """
    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'actions',
        'rewards',
    )

    @staticmethod
    def calculate(trials):
        all_data = pd.concat(trials)
        data_by_rounds = all_data.groupby('round')

        cum_count_actions = pd.DataFrame(columns=np.arange(0, len(data_by_rounds.first()["rewards"].values[0])))
        cumulative_counter = Counter([])

        for i, cur_round in data_by_rounds:
            b = cur_round["actions"].tolist()
            c = []
            for j in b:
                c += list(j)
            cumulative_counter += Counter(c)
            cum_count_actions = cum_count_actions.append(dict(cumulative_counter), ignore_index=True)

        cum_count_actions.columns = ['cum_count_actions_' + str(i) for i in range(len(cum_count_actions.columns))]

        cum_sum_rewards = []
        for i, cur_round in data_by_rounds:
            c = np.sum(cur_round["rewards"].tolist(), axis=0)
            cum_sum_rewards.append(c)
        cum_sum_rewards = pd.DataFrame(np.cumsum(cum_sum_rewards, axis=0))
        cum_sum_rewards.columns = ['cum_sum_rewards_' + str(i) for i in range(len(cum_sum_rewards.columns))]

        return [
            pd.concat([cum_count_actions, cum_sum_rewards], axis=1)
        ]
