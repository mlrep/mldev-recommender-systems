# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'expected_adversarial_regret'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class ExpectedAdversarialRegret(Metric):
    """The expected regret for adversarial environments.

    https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/Основы-многоруких-бандитов#ожидаемый-регрет
    """

    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'selected_action_indexes',
        'all_action_rewards',
    )

    @staticmethod
    def calculate(trials):
        random_regrets = []
        for trial_df in trials:
            trial_df = trial_df.sort_values('round')
            selected_action_count = len(trial_df.iloc[0]['selected_action_indexes'])

            optimal_action_indexes = np.argpartition(
                trial_df['all_action_rewards'].sum(),
                -selected_action_count
            )[-selected_action_count:]

            trial_df[METRIC_NAME] = trial_df.apply(
                lambda row: (
                    np.around(
                        sum(row['all_action_rewards'][optimal_action_indexes])
                        - sum(row['all_action_rewards'][row['selected_action_indexes']]),
                        5,
                    )
                ),
                axis=1,
            )
            trial_df[METRIC_NAME] = trial_df[METRIC_NAME].cumsum()
            random_regrets.append(
                pd.DataFrame(trial_df[[ROUND_COLUMN_NAME, METRIC_COLUMN_NAME]])
            )
        return [
            pd.concat(random_regrets).groupby('round').mean(),
        ]


if __name__ == '__main__':
    regret = ExpectedAdversarialRegret(
        data_path=(
            './results/exp3-experiment/49/trial-num:*/data-*.parquet'
        ),
        result_path=(
            './results/exp3-experiment/49'
        ),
        columns_mapping={
            'selected_action_indexes': lambda row: row['actions'],
            'all_action_rewards': lambda row: row['reward'],
        },
    )

    import time
    start = time.time()
    regret()
    end = time.time()
    print('Execution time: ', end - start)
