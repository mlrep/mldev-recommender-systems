# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'actions_probability'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class ActionsProbability(Metric):
    """The probability of choosing the actions on each round.

    Averaging is performed over the trials.
    """

    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'actions_vector',
    )

    @staticmethod
    def calculate(trials):
        grp = pd.concat(trials).groupby(ROUND_COLUMN_NAME)
        series = grp['actions_vector'].apply(
            lambda x: np.array(x.tolist()).mean(axis=0)
        )
        result_df = pd.DataFrame({
            'round': series.index,
            METRIC_COLUMN_NAME: series.values,
        }).set_index('round')
        return [result_df]


if __name__ == '__main__':
    regret = ActionsProbability(
        data_path=(
            './results/exp3-experiment/49/trial-num:*/data-*.parquet'
        ),
        result_path=(
            './results/simple-experiment/49/'
        ),
        columns_mapping={
            'actions_vector': lambda row: np.array(
                [1 if i in row['actions'] else 0 for i in range(3)]
            ),
        },
    )

    import time
    start = time.time()
    regret()
    end = time.time()
    print('Execution time: ', end - start)
