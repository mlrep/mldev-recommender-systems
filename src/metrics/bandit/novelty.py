# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'novelty'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class Novelty(Metric):
    """The cumulative novelty of choosing the actions on each round.

    Averaging is performed over the trials and users.

    [1] Wang L. et al. Biucb: A contextual bandit algorithm for cold-start and diversified recommendation //2017 IEEE
        International Conference on Big Knowledge (ICBK). – IEEE, 2017. – С. 248-253.
    """
    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'actions',
    )

    @staticmethod
    def calculate(trials):
        trial_novelties = []
        for trial_df in trials:
            def f(row):
                actions = set(row['actions'])
                recommended_actions_set = row['recommended_actions']
                novelty = len(actions.difference(recommended_actions_set)) / len(actions)
                return novelty

            trial_df['recommended_actions'] = trial_df['actions'].apply(lambda x: list(x)) \
                                                                 .cumsum() \
                                                                 .apply(set) \
                                                                 .shift(1, fill_value=set())
            trial_df = trial_df.sort_values('round')
            trial_df[METRIC_COLUMN_NAME] = trial_df.apply(f, axis=1)
            trial_df[METRIC_COLUMN_NAME] = trial_df['novelty'].cumsum() / (trial_df['round'] + 1)

            trial_novelties.append(
                pd.DataFrame(trial_df[[ROUND_COLUMN_NAME, METRIC_COLUMN_NAME]])
            )
        return [
            pd.concat(trial_novelties).groupby('round').mean(),
        ]
