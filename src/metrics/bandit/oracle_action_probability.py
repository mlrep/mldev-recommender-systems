# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'oracle_action_probability'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class OracleActionProbability(Metric):
    """The probability of choosing the optimal actions on each round.

    Averaging is performed over the trials.
    """

    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'selected_action_indexes',
        'all_action_rewards',
    )

    @staticmethod
    def calculate(trials):
        random_regrets = []
        for trial_df in trials:
            def f(row):
                actions_reward = row['all_action_rewards'][row['selected_action_indexes']].sum()
                largest_reward = np.sort(row['all_action_rewards'])[-len(row['selected_action_indexes']):].sum()
                return int(actions_reward == largest_reward)

            trial_df['optimal_action_count'] = trial_df.apply(f, axis=1)
            random_regrets.append(
                pd.DataFrame(trial_df[[ROUND_COLUMN_NAME, 'optimal_action_count']])
            )
        result_df = pd.concat(random_regrets).groupby(ROUND_COLUMN_NAME).sum()
        result_df[METRIC_COLUMN_NAME] = (
            result_df['optimal_action_count'].div(len(random_regrets))
        )
        del result_df['optimal_action_count']
        return [result_df]


if __name__ == '__main__':
    regret = OracleActionProbability(
        data_path=(
            './results/simple-experiment/49/data-0.parquet'
        ),
        result_path=(
            './results/simple-experiment/49/'
        ),
        columns_mapping={
            'selected_action_indexes': lambda row: row['actions'],
            'all_action_rewards': lambda row: row['rewards'],
        },
    )

    import time
    start = time.time()
    regret()
    end = time.time()
    print('Execution time: ', end - start)
