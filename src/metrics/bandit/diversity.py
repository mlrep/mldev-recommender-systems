# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'diversity'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class Diversity(Metric):
    """The cumulative diversity of choosing the actions on each round.

    Averaging is performed over the trials and users.

    [1] Wang L. et al. Biucb: A contextual bandit algorithm for cold-start and diversified recommendation //2017 IEEE
        International Conference on Big Knowledge (ICBK). – IEEE, 2017. – С. 248-253.
    """
    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'actions',
    )

    @staticmethod
    def calculate(trials):
        trial_diversities = []
        for trial_df in trials:
            def f(row):
                actions = set(row['actions'])
                actions_lag_1 = set(row['actions_lag_1'])
                if len(actions_lag_1):
                    return len(actions.difference(actions_lag_1)) / len(actions_lag_1)
                else:
                    return 1

            trial_df['actions_lag_1'] = trial_df['actions'].shift(1, fill_value=set())

            trial_df = trial_df.sort_values('round')
            trial_df[METRIC_COLUMN_NAME] = trial_df.apply(f, axis=1)
            trial_df[METRIC_COLUMN_NAME] = trial_df['diversity'].cumsum() / (trial_df['round'] + 1)

            trial_diversities.append(
                pd.DataFrame(trial_df[[ROUND_COLUMN_NAME, METRIC_COLUMN_NAME]])
            )
        return [
            pd.concat(trial_diversities).groupby('round').mean(),
        ]
