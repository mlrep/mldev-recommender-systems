# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag

from stages.metrics.metric import Metric


METRIC_NAME = 'expected_stochastic_regret'
METRIC_COLUMN_NAME = METRIC_NAME
ROUND_COLUMN_NAME = 'round'


@experiment_tag()
class ExpectedStochasticRegret(Metric):
    """The expected regret for stochastic environments.

    https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/Основы-многоруких-бандитов#ожидаемый-регрет-expected-regret
    """

    metric_name = METRIC_NAME
    required_columns = (
        ROUND_COLUMN_NAME,
        'reward_distributions',
        'selected_action_count',
        'selected_action_reward',
    )

    @staticmethod
    def calculate(trials):
        random_regrets = []
        for trial_df in trials:
            def f(row):
                optimal_reward = sum(
                    sorted(row['reward_distributions'])[-row['selected_action_count']:]
                )
                return optimal_reward - row['selected_action_reward']

            trial_df = trial_df.sort_values('round')
            trial_df[METRIC_COLUMN_NAME] = trial_df.apply(f, axis=1)
            trial_df[METRIC_COLUMN_NAME] = trial_df[METRIC_COLUMN_NAME].cumsum()

            random_regrets.append(
                pd.DataFrame(trial_df[[ROUND_COLUMN_NAME, METRIC_COLUMN_NAME]])
            )
        return [
            pd.concat(random_regrets).groupby('round').mean(),
        ]


if __name__ == '__main__':
    regret = ExpectedStochasticRegret(
        data_path=(
            './results/exp3-experiment/49/trial-num:*/data-*.parquet'
        ),
        result_path=(
            './results/exp3-experiment/49/'
        ),
        columns_mapping={
            'reward_distributions': lambda row: [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.8, 0.9, 0.1, 0.1],
            'selected_action_count': lambda row: len(row['reward'][row['actions']]),
            'selected_action_reward': lambda row: sum(row['reward'][row['actions']]),
        },
    )

    import time
    start = time.time()
    regret()
    end = time.time()
    print('Execution time: ', end - start)
