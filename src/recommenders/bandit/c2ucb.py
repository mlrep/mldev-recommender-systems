# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
C2UCB
======

An algorithm for the task of contextual stochastic multi-armed bandits based on the use of an external data set
consisting of feature descriptions of actions at each round. The assumption is introduced that at each round
the expected reward for an action linearly depends on its description. Thus, the linear regression problem is solved.

The main idea is an approach called contextual combinatorial bandit in which a learning algorithm can dynamically
identify diverse actions that interest a new user. Specifically, each actions is represented as a feature vector, and
each user is represented as an unknown preference vector. On each of n rounds, the bandit algorithm sequentially
selects a set of actions according to the action-selection algorithm that balances exploration and exploitation, and
collects the user feedback on these selected actions.

References
----------
.. [1] Qin L., Chen S., Zhu X. Contextual combinatorial bandit and its application on diversified online recommendation
       //Proceedings of the 2014 SIAM International Conference on Data Mining. – Society for Industrial and
       Applied Mathematics, 2014. – С. 461-469.
"""

import numpy as np
from numpy.linalg import pinv, norm


def init(lmbda, d, use_cosine_regularization=False):
    """
    Algorithm initialization.
    :param lmbda: Regularization parameter of users preferences.
    :param d: Number of features in context matrix.
    :param use_cosine_regularization: Flag when to use cosine regularization (it is a part of our improvement).
    :return: initialized parameters
    """
    if use_cosine_regularization:
        return lmbda * np.eye(N=d), np.zeros((d, 1)), set()
    else:
        return lmbda * np.eye(N=d), np.zeros((d, 1))


def predict(V, b, action_features, alpha, l, lmbda, sigma, d,
            use_cosine_regularization=False, cosine_lambda=None,
            viewed_actions=None, actions_num=None):
    """
    Forecast actions at current round.
    :param V: Algorithm matrix with shape (d, d).
    :param b: Bias vector with shape (d).
    :param action_features: Context features per each action with shape (action_size, d). Should be normalized.
    :param alpha: Regularization hyperparameter.
    :param l: Top-l actions to recommend. Should be less than total number of allowed actions.
    :param lmbda: Regularization parameter of users preferences.
    :param sigma: Regularization parameter. According to algorithm authors sigma^2 should be great or equal than 0.0586.
    :param use_cosine_regularization: Flag when to use cosine regularization (it is a part of our improvement).
    :param cosine_lambda: Coefficient of cosine regularization (it is a part of our improvement).
    :param viewed_actions: Set of viewed actions.
    :param actions_num: Number of actions to use in cosine regularization.
    :param d: Feature vector dimension.
    :return: Vector of shape (l, ) that contains recommended actions indices
    """
    V_inv = pinv(V)
    theta = np.dot(V_inv, b)

    theta_norm = norm(theta)
    if theta_norm:
        theta /= theta_norm

    action_to_r_score = {}
    for action in action_features.keys():
        first_term = np.dot(theta.T, action_features[action].reshape(-1, 1))
        second_term = np.sqrt(np.dot(np.dot(action_features[action].reshape(1, -1), V_inv),
                                     action_features[action].reshape(-1, 1)))
        r_score = first_term + alpha * second_term

        if use_cosine_regularization:
            similarity = average_similarity(action_features, action, viewed_actions, actions_num)
            r_score -= cosine_lambda * similarity

        action_to_r_score[action] = r_score.reshape(-1)[0]

    rec_actions = get_super_action(action_to_r_score, action_features, l, lmbda, sigma, d)

    if viewed_actions is not None:
        viewed_actions.update(rec_actions)
        return rec_actions, viewed_actions
    else:
        return rec_actions


def get_super_action(action_to_r_score, action_features, l, lmbda, sigma, d):
    """
    Method that generates recommendations of size l
    :param action_to_r_score: Dictionary with action as key and its algorithm's reward as a value.
    :param action_features: Context features per each action with shape (action_size, d).
    :param l: Top-l actions to recommend.
    :param lmbda: Regularization parameter.
    :param sigma: Regularization parameter.
    :param d: Feature vector dimension.
    :return: Vector of shape (l, ) that contains recommended actions indices.
    """
    S = set()
    C = 1/sigma
    actions = set(action_features.keys())
    XS = np.array(np.zeros(d)).reshape(d, -1)
    N = len(actions)

    for j in range(l):
        delta_gs = np.zeros(N)
        rewards = np.zeros(N)
        d = {}
        for i, action in enumerate(set.difference(actions, S)):
            d[i] = action
            sigmas = np.dot(action_features[action], XS)
            rewards[i] = action_to_r_score[action]
            delta_gs[i] = 1/2 * np.log(2 * np.pi * np.e * (sigma + np.dot(sigmas, C).dot(sigmas.T)))
        S.add(d[np.argmax(rewards + lmbda * delta_gs)])
        XS = np.array([action_features[i] for i in S]).T
        C = np.linalg.inv(np.dot(XS.T, XS) + sigma * np.eye(len(S)))
    return list(S)


def update(V, b, action_features, reward):
    """
    Updating C2UCB parameters.
    :param V: Algorithm matrix with shape (d, d).
    :param b: Bias vector with shape (d).
    :param action_features: Context features per each action with shape (action_size, d).
    :param reward: Vector of reward per each action.
    :return: updated parameters
    """
    if (reward is None) or (len(reward) == 0):
        return V, b

    for action in reward.keys():
        V += np.dot(action_features[action], action_features[action].T)
        b += reward[action] * action_features[action].reshape(-1, 1)
    return V, b


def cosine_similarity(a, b):
    """
    Cosine similarity between two vectors.
    :param a: First vector.
    :param b: Second vector.
    :return: Cosine similarity between two given vectors.
    """
    return np.dot(a, b) / (np.linalg.norm(a) * np.linalg.norm(b))


def average_similarity(action_embeddings, action, viewed_actions, actions_num):
    """
    Mean cosine similarity between target vector and other vectors.
    :param action_embeddings: Feature vectors per actions.
    :param action: Target action.
    :param viewed_actions: Set of viewed actions in previous rounds.
    :param actions_num: Number of random viewed actions to average with.
    :return: Mean value of cosine similarity.
    """
    if len(viewed_actions) == 0:
        return 0

    random_actions = np.random.choice(list(viewed_actions), size=actions_num, replace=False)
    similarities = [cosine_similarity(action_embeddings[action], action_embeddings[random_action])
                    for random_action in random_actions]
    return np.mean(similarities)
