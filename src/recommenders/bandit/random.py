# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Random policy.

At each round this policy returns l random actions.
"""


import numpy as np


def predict(M, l):
    """
    Predicting actions to recommend.

    :param l: Number of actions that the algorithm takes on each round.
    :return: List with predicted actions with length equals to l.
    """
    return np.random.permutation(M)[:l]
