# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Exp3
====

Algorithm assigns weights to each handle, then, during the update, a more optimal
action is assigned a greater weight. The work process is arranged as follows:

 1. a softmax distribution is set above the actions;
 2. an action is randomly sampled from the resulting distribution;
 3. a selection reward is observed and weights are updated.

References
----------
.. [1] Auer, Peter and Cesa-Bianchi, Nicolo and Freund, Yoav and Schapire, Robert E
   "The nonstochastic multiarmed bandit problem",
   SIAM journal on computing, pp. 48--77, 2002, SIAM
"""

import numpy as np


def init_parameters(M):
    """
    Parameters initialization.

    :param M: Number of unique actions. 0 < M.
    :return: Init zero weights for each action.
    """
    if M <= 0:
        raise ValueError("Negative number of actions.")

    weights = np.ones(shape=M)
    return weights


def get_distribution(weights, gamma, M):
    """
    Form distribution over weights.

    :param weights: Weights per action. It is a learned parameter that updates each round.
                    Greater value of weight corresponds to more optimal action from the point
                    of view of the algorithm
    :param gamma: Hyperparameter that controls trade-off between exploration and exploitation. Should be 0 < gamma < 1.
    :param M: Number of unique actions. 0 < M.
    :return: Probabilistic distribution over weights.
    """
    distribution = (1 - gamma) * weights / np.sum(weights) + gamma / M
    return distribution


def predict(M, distribution):
    """
    Get actions based on their weights.

    :param M: Number of unique actions. 0 < M.
    :param distribution: Probabilistic distribution over weights.
    :return: Vector of shape (l, ) that contains recommended actions indices.
    """
    actions = np.random.choice(a=M, size=1, replace=False, p=distribution)
    return actions


def update_weights(weights, reward, distribution, actions, gamma, M):
    """
    Get actions based on their weights.

    :param weights: Weights per action. It is a learned parameter that updates each round.
                    Greater value of weight corresponds to more optimal action from the point
                    of view of the algorithm
    :param reward: Vector of reward per each action.
    :param distribution: Probabilistic distribution over weights.
    :param actions: Vector of recommended actions by the algorithm.
    :param gamma: Hyperparameter that controls trade-off between exploration and exploitation. Should be 0 < gamma < 1.
    :param M: Number of unique actions. 0 < M.
    :return: Updated weights.
    """
    if reward is None:
        return weights

    if any(i < 0 for i in reward):
        raise ValueError("Negative reward")

    weights[actions] *= np.exp(gamma * reward[actions] / (distribution[actions] * M))
    return weights
