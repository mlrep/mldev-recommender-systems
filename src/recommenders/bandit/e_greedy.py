# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
E-greedy algorithm.

An algorithm for the problem of competitive multi-armed bandits. The idea is that with probability E, a random action
is chosen. Accordingly, with probability (1 - E), the action that has the maximum average win in the current round is
greedily selected. The algorithm supports simultaneous selection of several actions on each round. The modification
consists in the fact that at each round a list of actions ranked by average winnings is selected.
"""

import numpy as np
import random


def init_parameters(M):
    """
    Parameters initialization.

    :param M: Number of unique actions. 0 < M.
    :return: tuple (Vector with rewards per each action,
                    Vector with counts of action choices over rounds,
                    Vector for algorithm params)
    """
    if M <= 0:
        raise ValueError("Negative number of actions.")

    action_rewards = np.zeros(shape=M)
    action_choices_count = np.zeros(shape=M)
    params = np.zeros(shape=M)
    return action_rewards, action_choices_count, params


def predict(params, M, l, epsilon):
    """
    Predicting actions to recommend.

    :param params: Vector with algorithm params
    :param M: Number of unique actions. 0 < M.
    :param l: Number of actions that the algorithm takes on each round. 0 < l <= M.
    :param epsilon: Probability of exploring phase.
    :return: Top-l actions according to algorithm parameters.
    """

    if random.random() > epsilon:
        return np.argsort(-params)[:l]
    else:
        return np.random.choice(a=M, size=l)


def update(action_rewards, action_choices_count, params, actions, reward):
    """
    Updating algorithm parameters.

    :param action_rewards: Vector with rewards per each action.
    :param action_choices_count: Vector with counts of action choices over rounds.
    :param params: Vector for algorithm params.
    :param actions: Vector of recommended actions by the algorithm.
    :param reward: Vector of reward per each action.
    :return: Updated parameters.
    """

    if (reward is None) or (len(reward) == 0):
        return action_rewards, action_choices_count, params

    if any(i < 0 for i in reward):
        raise ValueError("Negative reward")

    if isinstance(reward, dict):
        for action in reward.keys():
            action_rewards[action] += reward[action]
            action_choices_count[action] += 1
            params[action] = action_rewards[action] / action_choices_count[action]
            return action_rewards, action_choices_count, params
    else:
        action_rewards[actions] += reward[actions]
        action_choices_count[actions] += 1
        params[actions] = action_rewards[actions] / action_choices_count[actions]
        return action_rewards, action_choices_count, params
