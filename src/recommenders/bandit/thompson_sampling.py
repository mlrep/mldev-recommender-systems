# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Thompson sampling
=================

TS is a stochastic bandit, based on Beta-distribution. At each round, an action from the specified one is sampled,
a reward for its selection is observed, and the distribution parameters for this action are updated.

References
----------
.. [1] Russo, Daniel J and Van Roy, Benjamin and Kazerouni, Abbas and Osband, Ian and Wen, Zheng and others
   "A tutorial on thompson sampling", Foundations and Trends in Machine Learningc, vol. 11, num. 1,
   pp. 1-96, 2018, Now Publishers, Inc.
"""

import numpy as np


def init_parameters(M):
    """
    Initialization algorithm's parameters.

    :param M: Number of unique actions. 0 < M.
    :return: Initialized parameters of Beta-distribution over each action.
    """
    if M <= 0:
        raise ValueError("Negative number of actions.")

    params = np.zeros(shape=(M, 2))
    return params


def predict(l, params):
    """
    Predicting actions to recommend by sampling from Beta-distribution.

    :param params: Parameters of Beta-distribution over each action.
    :param l: Number of actions that the algorithm takes on each round.
    :return: Vector of shape (l, ) that contains recommended actions indices.
    """
    probs = np.random.beta(a=params[:, 0]+1, b=params[:, 1]+1)
    actions = np.argsort(-probs)[:l]
    return actions


def update(params, actions, reward):
    """
    Updating algorithm parameters using bandit's response.

    :param params: Parameters of Beta-distribution over each action.
    :param actions: Vector of recommended actions by the algorithm.
    :param reward: Vector of reward per each action.
    :return: Updated algorithm parameters.
    """
    # First round without the bandit response
    if (reward is None) or (len(reward) == 0):
        return params

    if any(i < 0 for i in reward):
        raise ValueError("Negative reward")

    if isinstance(reward, dict):
        for action in reward.keys():
            params[action] += np.vstack([reward[action], 1 - reward[action]]).T.reshape(-1)
            return params
    else:
        params[actions] += np.vstack([reward[actions], 1 - reward[actions]]).T
        return params
