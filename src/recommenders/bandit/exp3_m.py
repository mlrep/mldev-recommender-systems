# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""Exp3.M algorithm.
This algorithm is a modification over original Exp3 for multiple play scenario.

The algorithm  relies on the online mirror descent principle, a general methodology to minimize regret in online convex
optimization. More precisely, it can be seen as a particular case of the Online Stochastic Mirror Descent algorithm of
used with the negative entropy function on the convex set. Updating procedure looks like this -  to compute the new
parametrization vector at next round, the Exp3.M algorithm starts from current parametrization, then moves along the
direction of the estimated reward vector, and finally projects it back onto the convex set.

References
----------
.. [1] Louëdec J. et al. A multiple-play bandit algorithm applied to recommender systems
       //The Twenty-Eighth International Flairs Conference. – 2015.
"""

import numpy as np
from scipy.optimize import fsolve


def init_parameters(M, l):
    """
    Initialization of algorithm parameters.

    :param M: Number of unique actions. 0 < M.
    :param l: Number of actions that the algorithm takes on each round. 0 < l <= M.
    :return: initialized parameters per each action.
    """
    p = np.array([l / M for _ in range(M)])
    return p


def dependent_rounding(p, l):
    """
    Algorithm for sampling actions per each round.

    :param p: List with algorithm parameters per each action.
    :param l: Number of actions that the algorithm takes on each round.
    :return: Top-l actions based on parameters.
    """
    p_ = p.copy()
    while any(0 < i < 1 for i in p_):
        try:
            i, j = np.random.choice(np.where((p_ > 0) & (p_ < 1))[0], size=2, replace=False)
            alpha = min(1 - p_[i], p_[j])
            beta = min(p_[i], 1 - p_[j])

            p_[i], p_[j] = (p_[i] + alpha, p_[j] - alpha) if np.random.random() < beta/(alpha+beta) \
                else (p_[i] - beta, p_[j] + beta)
        except:
            break

    actions = np.where(p_ == 1)[0]
    if len(actions) == l:
        return actions
    else:
        return np.argsort(-p_)[:l]


def predict(p, l):
    """
    Prediction procedure to get l actions for recommendation.

    :param p: List with algorithm parameters per each action.
    :param l: Number of actions that the algorithm takes on each round.
    :return: Top-l actions based on parameters.
    """
    actions = dependent_rounding(p, l)
    return actions


def update(eta, p, reward, l, eps=1e-9):
    """
    Updating algorithm parameters.

    :param eta: Regularization hyperparameter.
    :param p: List with algorithm parameters per each action.
    :param reward: Vector of reward per each action.
    :param l: Number of actions that the algorithm takes on each round.
    :param eps: Constant that is added to algorithm parameters to avoid np.log(0).
    :return: Updated algorithm parameters.
    """
    if reward is None:
        return p

    if any(i < 0 for i in reward):
        raise ValueError("Negative reward")

    no_bias_reward = reward / (p + eps)
    t = np.log(p + eps) + eta * no_bias_reward
    q = softmax_modified(t) * l

    C = fsolve(func, x0=[0.5], args=(q, l))[0]
    p = np.minimum(C * q, 1)
    return p


def softmax_modified(x):
    """
    Corrected version of softmax procedure for large numbers based on log-sum-exp trick.

    :param x: Input list with parameters
    :return: Parameters after softmax procedure.
    """
    c = np.max(x)
    lse = c + np.log(np.sum(np.exp(x - c)))
    return np.exp(x - lse)


def func(C, q, l):
    """
    Function to be optimized in updating procedure.

    :param C: Variable to be optimized.
    :param q: Internal parameters of algorithm which are additional args for optimisation task.
    :param l: Number of actions that the algorithm takes on each round.
    :return: Value of the discrepancy in solving equation.
    """
    return np.sum(np.minimum(C * q, 1)) - l
