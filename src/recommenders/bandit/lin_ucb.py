# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
LinUCB
======

An algorithm for the task of contextual stochastic multi-armed bandits based on the use of an external data set
consisting of feature descriptions of actions at each round. The assumption is introduced that at each round
the expected reward for an action linearly depends on its description. Thus, the linear regression problem is solved.

At the initialization stage, a rectangular unit matrix A of size
(the number of actions, the size of the feature description vector) and the corresponding displacement vector b
are created. Then, at each round, based on the contextual matrix describing the external data set,
matrix A and vector b, estimates are calculated for each action, and the action with the maximum score is selected.
After observing the reward for the selected action, its parameters are updated.

References
----------
.. [1] Li L. et al. A contextual-bandit approach to personalized news article recommendation
   //Proceedings of the 19th international conference on World wide web. – 2010. – С. 661-670.
"""

import numpy as np
from numpy.linalg import pinv


def init_params(M, d):
    """
    Algorithm initialization.
    :param M: Number of unique actions. 0 < M.
    :param d: Number of features in context matrix.
    :return: initialized parameters
    """
    a_matrices = np.array([np.eye(N=d) for _ in range(M)])
    b_vectors = np.array([np.zeros(shape=(d, 1)) for _ in range(M)])
    return a_matrices, b_vectors


def predict(context_matrix, a_matrices, b_vectors, alpha, l):
    """
    Forecast actions at current round.
    :param context_matrix: Matrix of context with shape (M, d).
    :param a_matrices: A_matrices per each action with shape (M, d, d).
    :param b_vectors: b_vectors for each action with shape (M, d, 1).
    :param alpha: regularization hyperparameter.
    :param l: Top-l actions to recommend. Should be 0 < l <= M.
    :return: Vector of shape (l, ) that contains recommended actions indices.
    """
    a_matrices_inv = pinv(a_matrices)
    theta_vectors = a_matrices_inv @ b_vectors

    first_term = np.transpose(theta_vectors, axes=(0, 2, 1)) @ context_matrix[:, :, np.newaxis]
    second_term = (np.transpose(context_matrix[:, :, np.newaxis], axes=(0, 2, 1)) @ \
                   (a_matrices_inv @ context_matrix[:, :, np.newaxis])) ** (1 / 2)

    p_scores = (first_term + alpha * second_term).reshape(-1)
    actions = np.argsort(-p_scores)[:l]
    return actions


def update(a_matrices, b_vectors, context_matrix, reward, actions):
    """
    Updating LinUCB parameters.
    :param a_matrices: A_matrices per each action with shape (M, d, d).
    :param b_vectors: b_vectors for each action with shape (M, d, 1).
    :param context_matrix: Matrix of context with shape (M, d).
    :param reward: Vector of reward per each action.
    :param actions: Vector of recommended actions by the algorithm.
    :return: updated algorithm parameters.
    """
    # First round without the bandit response
    if reward is None or context_matrix is None:
        return a_matrices, b_vectors

    for action in actions:
        a_matrices[action] += context_matrix[action, :].reshape(1, -1).T @ context_matrix[action, :].reshape(1, -1)
        b_vectors[action] += reward[action] * context_matrix[action, :].reshape(1, -1).T
    return a_matrices, b_vectors

