# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Optimal policy.

At each round this policy returns top-l best actions based on internal bandit's reward vector.
"""


import numpy as np


def predict(parametrization, M, l):
    """
    Predicting actions to recommend.

    :param l: Number of actions that the algorithm takes on each round.
    :param parametrization: Vector of Bernoulli parameters per each action.
    :return: List with predicted actions with length equals to l.
    """

    if parametrization is None:
        return np.random.permutation(M)[:l]
    else:
        return np.argsort(a=-parametrization)[:l]
