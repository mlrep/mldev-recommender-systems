# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
f-dsw-ts
========

The algorithm is a development of the idea of TS. The concepts of time window and discounting are added.
Such parameters allow you to take into account only the recent distribution of awards by handles,
which allows you to catch the change of user interests.

References
----------
.. [1] Cavenaghi, Emanuele and Sottocornola, Gabriele and Stella, Fabio and Zanker, Markus
   "Non-stationary multi-armed bandit: Empirical evaluation of a new concept drift-aware algorithm",
   Entropy, vol. 23, num. 3, p. 380, 2021, MDPI
"""

import numpy as np


def init_parameters(M):
    """
    Initialization of algorithm parameters.

    :param M: Number of unique actions. 0 < M.
    :return: tuple (Parameters of Beta-distribution based on all previous rounds,
                    Parameters of Beta-distribution based on last window_size rounds,
                    Dictionary that contains last window_size values of rewards per each action).
    """
    if M <= 0:
        raise ValueError("Negative number of actions.")

    historic_trace_params = np.ones(shape=(M, 2))
    hot_trace_params = np.ones(shape=(M, 2))
    last_rewards_per_action = {a: [] for a in range(M)}

    return historic_trace_params, hot_trace_params, last_rewards_per_action


def predict(historic_trace_params, hot_trace_params, M, l):
    """
    Predict actions based on their historic and hot traces.

    :param historic_trace_params: Parameters of Beta-distribution based on all previous rounds.
    :param hot_trace_params: Parameters of Beta-distribution based on last window_size rounds.
    :param M: Number of unique actions. 0 < M.
    :param l: Top-l actions to recommend. Should be 0 < l <= M.
    :return: Vector of shape (l, ) that contains recommended actions indices.
    """
    historic_trace = [np.random.beta(
        a=historic_trace_params[action][0] + 1, b=historic_trace_params[action][1] + 1) for action in range(M)]

    hot_trace = [np.random.beta(
        a=hot_trace_params[action][0], b=hot_trace_params[action][1]) for action in range(M)]

    actions = np.argsort([np.max([historic_trace[action], hot_trace[action]]) for action in range(M)])[-l:]
    return actions


def update(actions, reward, hot_trace_params, historic_trace_params, last_rewards_per_action, gamma, window_size, w, M):
    """
    Updating parameters according to user response.

    :param actions: Vector of recommended actions by the algorithm.
    :param reward: Vector of reward per each action.
    :param hot_trace_params: Parameters of Beta-distribution based on last window_size rounds.
    :param historic_trace_params: Parameters of Beta-distribution based on all previous rounds.
    :param last_rewards_per_action: Dictionary that contains last window_size values of rewards per each action.
    :param gamma: Hyperparameter that controls the power of discounting rewards at previous rounds.
    :param window_size: Hyperparameter that controls the amount of last rounds in hot trace.
    :param w: Additive noise
    :return: tuple (Parameters of Beta-distribution based on all previous rounds,
                    Parameters of Beta-distribution based on last window_size rounds,
                    Dictionary that contains last window_size values of rewards per each action).
    """
    # First round without the user response
    if reward is None:
        return historic_trace_params, hot_trace_params, last_rewards_per_action

    if any(i < 0 for i in reward):
        raise ValueError("Negative reward")

    # action_reward = reward[actions]

    # Hot trace params update
    for action in actions:
        if len(last_rewards_per_action[action]) >= window_size:
            hot_reward = last_rewards_per_action[action].pop(0)
            hot_trace_params[action] -= np.hstack([hot_reward, 1 - hot_reward])

        hot_trace_params[action] += np.hstack([reward[action], 1 - reward[action]])
        last_rewards_per_action[action].append(reward[action])

        # Historic trace params update
        historic_trace_params *= gamma
        historic_trace_params[action] += np.hstack([reward[action], 1 - reward[action]])

    historic_trace_params += np.random.random(size=(M, 2)) * w
    hot_trace_params += np.random.random(size=(M, 2)) * w

    return historic_trace_params, hot_trace_params, last_rewards_per_action
