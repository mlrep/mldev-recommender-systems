# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
A Bernoulli reward distribution
===============================

A Bernoulli reward distribution with a constant vector of parametrization.

References
----------
.. [1] Russo, Daniel J and Van Roy, Benjamin and Kazerouni, Abbas and Osband, Ian and Wen, Zheng and others
   "A tutorial on thompson sampling", Foundations and Trends in Machine Learning, vol. 11, num. 1,
   pp. 1-96, 2018, Now Publishers, Inc.
"""


import numpy as np
from environments.bandit.base import get_actions_reward


def sample_rewards(parametrization):
    """
    Updating rewards. At each round vector of rewards re-sampled by given parametrization.

    :param parametrization: Vector of Bernoulli parameters per each action.
    :return: Vector with rewards over all actions.
    """

    rewards = np.random.binomial(n=1, p=parametrization, size=len(parametrization))
    return rewards
