# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Gaussian reward distribution
============================

Rewards are generated for each action as random variables from a multivariate
normal distribution (Gaussian distribution). At each round, the vector of
rewards is sampled based on the given parameterization.
"""


import numpy as np
from environments.bandit.base import get_actions_reward


def sample_rewards(mean, cov):
    """
    Sampling rewards. At each round vector of rewards re-sampled by given parametrization.

    :param mean: Vector of means per each action.
    :param cov: Matrix of covariances between actions with shape (number of actions, number of actions).
    :return: Vector with rewards over all actions.
    """

    rewards = np.random.multivariate_normal(mean=mean, cov=cov, size=1)
    return rewards
