# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Shared components of bandit environments
========================================

Shared components of bandit environments for further extension.
"""

import numpy as np


def get_actions_reward(actions, rewards):
    """
    Response to actions received from the algorithm.

    :param actions: Vector of recommended actions by the algorithm.
    :param rewards: Vector of rewards.
    :return: Vector of size equal to vector of rewards with reward values per each action chosen by algorithm.
    """
    if type(rewards) == list:
        rewards = np.array(rewards)

    actions_reward = np.zeros(shape=len(rewards))
    actions_reward[actions] = rewards[actions]
    return actions_reward
