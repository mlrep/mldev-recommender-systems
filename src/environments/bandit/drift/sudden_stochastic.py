# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
A Bernoulli reward distribution with sudden optimal action change
=================================================================

The Bernoulli reward distribution is used to simulate sudden change in the
distribution parametrization. At a specific round, a non-optimal action is
randomly chosen from all actions. Subsequently, the selected action becomes
the optimal action, and its Bernoulli parameter exceeds the parameter of the
previous optimal action by a given value called eps.

References
----------
.. [1] Cavenaghi, Emanuele and Sottocornola, Gabriele and Stella, Fabio and Zanker, Markus
   "Non-stationary multi-armed bandit: Empirical evaluation of a new concept drift-aware algorithm",
   Entropy, vol. 23, num. 3, p. 380, 2021, MDPI
"""


import numpy as np
from environments.bandit.base import get_actions_reward


def update_parametrization(current_round, parametrization, abrupt_time, eps):
    """
    Updating parametrization. At round equal to abrupt_time the optimal action is changed.

    :param current_round: Current iteration of experiment.
    :param parametrization: Vector of Bernoulli parameters per each action.
    :param abrupt_time: Round when the optimal action is changed.
    :param eps: Value by which the reward for the new optimal action will exceed the previous optimal action.
    :return: Updated parametrization.
    """
    if eps <= 0:
        raise ValueError("Negative epsilon!")

    if current_round == abrupt_time:
        sudden_action = np.random.choice(np.argsort(parametrization)[:-1])
        parametrization[sudden_action] += (np.max(parametrization) - parametrization[sudden_action]) + eps
    return parametrization


def sample_rewards(parametrization):
    """
    Sample rewards from Bernoulli distribution. At each round, actions can be randomly selected for which the
    Bernoulli parameter will be resampled using U[0, 1].

    :param parametrization: Vector of Bernoulli parameters per each action.
    :return: Vector with rewards over all actions.
    """

    rewards = np.array([np.random.binomial(1, p) for p in parametrization])
    return rewards
