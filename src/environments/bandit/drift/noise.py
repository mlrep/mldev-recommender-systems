# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
A Bernoulli reward distribution with noise
==========================================

Recommender systems are susceptible to the feedback closed loop effect, which
can cause the set of actions issued by the algorithm to degenerate. To address
this issue, the approach involves introducing evenly distributed noise to the
rewards, using actions that correspond to the algorithm's actions. This
noise serves to mitigate the impact of the feedback closed loop effect and
maintain the diversity of actions taken by the algorithm. Response rewards are
sampled from Bernoulli distribution.

References
----------
.. [1] A. Khritankov, A. Pilkevich "Existence conditions for
   hidden feedback loops in online recommender systems", arXiv,
   2109.05278, 2021

"""

import numpy as np
import scipy.special as special


def init(M):
    """
    Initialization of the distribution.

    :param M: Number of unique actions. 0 < M.
    :return: Vector of distribution parameters.
    """
    return np.random.uniform(low=-1, high=1, size=M)


def update_parametrization(actions, reward, parametrization, M, l):
    """
    Update the distribution. At each round to the current parametrization an
    exponential smoothing of rewards is added.

    :param actions: Vector of recommended actions by the algorithm.
    :param reward: Vector of reward per each action.
    :param parametrization: Vector of Bernoulli parameters per each action.
    :param M: Number of unique actions. 0 < M.
    :param l: Number of actions that the algorithm takes on each round. 0 < l <= M.
    :return: Updated parametrization.
    """

    delta = np.random.uniform(low=0, high=0.01, size=l)
    new_parametrization = np.zeros(M)
    new_parametrization[actions] += (reward[actions] * delta - delta * (1 - reward[actions]))
    parametrization += new_parametrization
    return parametrization


def get_actions_reward(parametrization, actions, M, w):
    """
    Response to actions received from the algorithm.

    :param parametrization: Vector of Bernoulli parameters per each action.
    :param actions: Vector of recommended actions by the algorithm.
    :param M: Number of unique actions. 0 < M.
    :param w: Additive noise to parametrization.
    :return: Vector of size equal to vector of rewards with reward values per each action chosen by algorithm.
    """

    all_reward = get_all_actions_reward(parametrization, M, w)
    reward = np.zeros_like(all_reward)
    reward[actions] = all_reward[actions]
    return reward


def get_all_actions_reward(parametrization, M, w):
    """
    Response on actions received from algorithm with all rewards.

    :param parametrization: Vector of Bernoulli parameters per each action.
    :param M: Number of unique actions. 0 < M.
    :param w: Additive noise to parametrization.
    :return: Vector of size equal to vector of rewards with reward values per each action chosen by algorithm.
    """

    noise_parametrization = parametrization + np.random.uniform(low=-w, high=w, size=M)
    return np.random.binomial(n=1, p=special.expit(noise_parametrization))
