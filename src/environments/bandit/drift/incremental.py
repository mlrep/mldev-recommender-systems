# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
A Bernoulli reward distribution with incremental probabilities
==============================================================

A Bernoulli reward distribution with incrementally increasing probabilities
of rewards for one of the actions as the process advances.

References
----------
.. [1] Cavenaghi, Emanuele and Sottocornola, Gabriele and Stella, Fabio and Zanker, Markus
   "Non-stationary multi-armed bandit: Empirical evaluation of a new concept drift-aware algorithm",
   Entropy, vol. 23, num. 3, p. 380, 2021, MDPI
"""


import numpy as np
from environments.bandit.base import get_actions_reward


def init(M):
    """
    Initialization of the distribution.

    :param M: Number of unique actions. 0 < M.
    :return: (Vector of distribution parameters, Index of action to become new optimal action)
    """

    parametrization = np.random.uniform(low=0.0, high=1.0, size=M)
    inc_action = np.random.choice(np.argsort(parametrization)[:-1])
    return parametrization, inc_action


def update_parametrization(current_round, parametrization, start_round, end_round, eps, increment, inc_action):
    """
    Update the distribution. Increase the value of the parameter corresponding to the action
    associated with inc_action by an increment at each round.

    :param current_round: Current iteration of experiment.
    :param rewards: Vector of rewards.
    :param start_round: Number of round when increment will start.
    :param end_round: Number of round when increment will end.
    :param eps: Value by which the reward for the new optimal action
                will exceed the previous optimal action.
    :param increment: Value on which reward for new optimal action should increase.
    :param inc_action: Index of action to become new optimal one.
    :return: (Updated parametrization, increment value to be added).
    """
    if eps <= 0:
        raise ValueError("Negative epsilon!")

    if current_round == 0:
        sort_actions = np.argsort(parametrization)
        increment = parametrization[sort_actions[-1]] - parametrization[inc_action]
    elif start_round <= current_round < end_round:
        parametrization[inc_action] += (increment + eps) / (end_round - start_round)
    return parametrization, increment


def sample_rewards(parametrization):
    """
    Sample rewards from Bernoulli distribution.

    :param parametrization: Vector of Bernoulli parameters per each action.
    :return: Vector with rewards over all actions.
    """
    rewards = np.array([np.random.binomial(1, p) for p in parametrization])
    return rewards
