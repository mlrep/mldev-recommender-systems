# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
A Bernoulli reward distribution with drift
==========================================

At each round, a Bernoulli distribution is used to select which reward
distribution parameters will be updated. The selected parameters are then
resampled using the uniform distribution U[0, 1]. This process of parameter
resampling represents the drift. Subsequently, the rewards are sampled
based on the new parameter values, following a Bernoulli distribution.

References
----------
.. [1] Cavenaghi, Emanuele and Sottocornola, Gabriele and Stella, Fabio and Zanker, Markus
   "Non-stationary multi-armed bandit: Empirical evaluation of a new concept drift-aware algorithm",
   Entropy, vol. 23, num. 3, p. 380, 2021, MDPI
"""


import numpy as np
from environments.bandit.base import get_actions_reward


def sample_rewards(parametrization, drift_prob):
    """
    Sample rewards from a Bernoulli distribution. At each round, random
    parameters are resampled using U[0, 1].

    :param parametrization: Vector of Bernoulli parameters per each action.
    :param drift_prob: Value of drift probability. Should be between 0.0 and 1.0
    :return: (Vector with rewards over all actions, Vector with Bernoulli parameters per each action)
    """

    shifts = np.random.binomial(n=1, p=drift_prob, size=len(parametrization))
    change_mask = np.where(shifts == 1)

    if len(change_mask) > 0:
        parametrization = np.array(parametrization)
        parametrization[change_mask] = np.random.uniform(low=0.0, high=1.0, size=len(change_mask))

    rewards = np.array([np.random.binomial(1, p) for p in parametrization])
    return rewards, parametrization
