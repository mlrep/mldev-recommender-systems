# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
A suddenly changed Bernoulli reward distribution
================================================

A Bernoulli reward distribution is used to simulate an abrupt moment when
the parameters of the distribution change to the explicitly defined values.

References
----------
.. [1] Cavenaghi, Emanuele and Sottocornola, Gabriele and Stella, Fabio and Zanker, Markus
   "Non-stationary multi-armed bandit: Empirical evaluation of a new concept drift-aware algorithm",
   Entropy, vol. 23, num. 3, p. 380, 2021, MDPI
"""


import numpy as np
from environments.bandit.base import get_actions_reward


def sample_rewards(parametrization, abrupt_parametrization, current_round, abrupt_time):
    """
    Sample rewards from Bernoulli distribution. At each round, actions can be randomly selected for which the
    Bernoulli parameter will be resampled using U[0, 1].

    :param parametrization: Vector of Bernoulli parameters per each action.
    :param abrupt_parametrization: Vector with Bernoulli parameters per each action to be set after abrupt time.
    :param current_round: Current iteration of experiment.
    :param abrupt_time: Round when parametrization will changed to abrupt_parametrization.
    :return: Vector with rewards over all actions.
    """

    if current_round >= abrupt_time:
        parametrization = abrupt_parametrization
    rewards = np.array([np.random.binomial(1, p) for p in parametrization])
    return rewards
