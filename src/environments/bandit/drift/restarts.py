# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
A Bernoulli reward distribution with restarts
=============================================

Recommender systems are susceptible to the feedback closed loop effect, which
can cause the set of actions issued by the algorithm to degenerate. To address
this issue, a re-initialization method is employed for the vector of
parametrization, using actions that correspond to the algorithm's
actions. This restarts serves to mitigate the impact of the feedback closed
loop effect and maintain the diversity of actions taken by the algorithm.
Response rewards are sampled from Bernoulli distribution.

References
----------
.. [1] A. Khritankov, A. Pilkevich "Existence conditions for
   hidden feedback loops in online recommender systems", arXiv,
   2109.05278, 2021

"""

import numpy as np
import scipy.special as special
from environments.bandit.drift.noise import (
    update_parametrization as noise_update_parametrization
)


def restart_parametrization(M):
    """
    Re-initialization of the distribution.

    :param M: Number of unique actions. 0 < M.
    :return: Vector of distribution parameters.
    """
    np.random.seed(0)
    return np.random.uniform(low=-1, high=1, size=M)


def update_parametrization(actions, reward, init_parametrization, parametrization, s, q, M, l):
    """
    Update the distribution with probability q parametrization restarts. Otherwise,
    random noise will be added.

    :param actions: Vector of recommended actions by the algorithm.
    :param reward: Vector of reward per each action.
    :param parametrization: Vector of Bernoulli parameters per each action.
    :param s: Scale factor.
    :param q: Probability of restart.
    :param M: Number of unique actions. 0 < M.
    :param l: Number of actions that the algorithm takes on each round. 0 < l <= M.
    :return: Vector of distribution parameters.
    """
    mask = np.random.uniform(size=M) <= q
    restart = init_parametrization.copy()
    scale = np.ones(M)
    scale[mask] = s

    parametrization = noise_update_parametrization(actions, reward, parametrization, M, l)

    index_mask = np.zeros(M)
    index_mask[mask] = 1
    new_parametrization = parametrization * (1 - index_mask) + \
                          ((1 - scale) * restart + scale * parametrization) * index_mask
    return new_parametrization


def get_actions_reward(parametrization, actions, M, w):
    """
    Response to actions received from the algorithm.

    :param parametrization: Vector of Bernoulli parameters per each action.
    :param actions: Vector of recommended actions by the algorithm.
    :param M: Number of unique actions. 0 < M.
    :param w: Additive noise to parametrization.
    :return: Vector of size equal to vector of rewards with reward values
        per each action chosen by algorithm.
    """
    noise_parametrization = parametrization + np.random.uniform(low=-w, high=w, size=M)
    all_reward = np.random.binomial(n=1, p=special.expit(noise_parametrization))
    reward = np.zeros_like(noise_parametrization)
    reward[actions] = all_reward[actions]
    return reward
