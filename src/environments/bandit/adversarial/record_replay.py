# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Module for records replay
=========================

Module for records replay simulates replaying the given records of the user's
actions.
"""


import numpy as np
import pandas as pd
from sklearn.preprocessing import OrdinalEncoder


def init(data_path, user_id, action_column, user_column):
    """
    Initialization of records.

    :param data_path: Path to data contains records.
    :param user_id: ID of the user whose actions will be replayed.
    :param action_column: Name of the column that identifies actions.
    :param user_column: Name of the column that identifies users.
    :return: (Sequence of actions, Object that encodes uer actions to [0, 1, ..., N], where N - number of
             unique actions.)
    """
    data = pd.read_csv(data_path, delimiter=';')
    records = data[data[user_column] == user_id][action_column].values.reshape(-1, 1)
    encoder = OrdinalEncoder(dtype=np.int32)
    records = encoder.fit_transform(records).reshape(-1)
    return records, encoder


def replay_record(records, current_round):
    """
    Replay record at the current round.

    :param records: Sequence of actions.
    :param current_round: Current iteration of experiment.
    :return: Replayed actions at current round.
    """

    return records[current_round]


def get_actions_reward(M, actions, replay_action):
    """
    Response according to algorithm predicted actions.

    :param M: Number of unique actions. 0 < M.
    :param actions: Vector of recommended actions by the algorithm.
    :param replay_action: Action that was taken.
    :return: Reward value for algorithm choice. Can be 0 if algorithm doesn't choose proper action else 1.
    """

    reward = np.zeros(shape=M)
    reward[replay_action] = 1 if replay_action in actions else 0
    return reward
