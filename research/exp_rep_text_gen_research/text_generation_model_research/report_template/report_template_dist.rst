Analysis of Epsilon-Greedy with non-stationarity
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Introduction
=============================================================================

The multi-armed bandit problem has been well studied for real-world
problems. In this problem, the agent chooses the best action to
should perform at time step t (round), based on past
rewards received from the environment. The formulation
implicitly assumes that the expected reward for each action
remains unchanged in the environment over time. Nevertheless
However, in many real-world applications, the agent has to face a
nonstationary environment, i.e., with a changing distribution
reward.

Experiment goal
====================================

Analyze the behavior of the Epsilon-Greedy algorithm for
non-stationarity

Research Method
=====================================

Conduct an experiment on the MLDev bench using the
Epsilon-Greedy and the sudden bandit.

Experiment description
===================================

General description of the experiment:

    {{  stage_params  }}

Interaction of algorithm and environment:

    {{  trial_run_script.code  }}

Changing the award distribution parameters:

    {{  bandit_run_script.code  }}

The environment description:

    {{  bandit  }}

And also the algorithm description:

    {{  algorithm  }}

The outputs of the experiment is

    {{  trial_outputs  }}


Results
======================================================

The detected non-stationarity has an effect on the
Epsilon-Greedy algorithm. Increasing the "duration of action" of the nonstationarity
significantly affects the deterioration of the behavior of the algorithm.