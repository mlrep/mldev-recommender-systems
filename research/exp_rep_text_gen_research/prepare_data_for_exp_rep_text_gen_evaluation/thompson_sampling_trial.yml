# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

common_params: &common_params
  M: 4
  l: 1


thompson_sampling_algorithm: &thompson_sampling !Atom
  name: Thompson Sampling
  <<: *common_params
  init_params: !function src/recommenders/bandit/thompson_sampling.init_parameters
  predict: !function src/recommenders/bandit/thompson_sampling.predict
  update: !function src/recommenders/bandit/thompson_sampling.update


sudden_bandit: &sudden_bandit !Atom
  name: Sudden Bandit
  <<: *common_params
  current_round: 0
  drift: ${env.DRIFT}
  abrupt_time_0: 0
  abrupt_time_1: 150
  abrupt_time_2: 300
  parametrization: [0.85, 0.85, 0.85, 0.9]
  parametrization_1: [0.01, 0.01, 0.01, 0.01]
  parametrization_2: [1.0, 0.0, 0.0, 0.0]
  sample_rewards: !function src/environments/bandit/drift/sudden.sample_rewards
  get_actions_reward: !function src/environments/bandit/drift/sudden.get_actions_reward
  run: !python-function |
      def run(current_round, actions):
          reward_distribution = parametrization
          if drift == 'True':
            if abrupt_time_0 <= current_round < abrupt_time_1:
              reward_distribution = parametrization_1
            elif abrupt_time_1 <= current_round < abrupt_time_2:
              reward_distribution = parametrization_2

          all_rewards = sample_rewards(reward_distribution, None, 0, 1)
          actions_rewards = get_actions_reward(actions, all_rewards)
    
          return all_rewards, actions_rewards, reward_distribution      
      return run

Trial_stage: &trial_stage  !TrialStage
  name: &trial_name text_generation_model_research_trial

  params:
    <<: *common_params
    trial: *trial_name
    bandit: *sudden_bandit
    algorithm: *thompson_sampling
    round_count: ${env.ROUNDCOUNT}

  inputs: &trial_inputs
    src: !path
      path: src
    config: !path
      path: ${env.EXPERIMENTFOLDER}/thompson_sampling_trial.yml

  outputs: &trial_outputs
    params: !path
      path: ${env.TARGETFOLDER}/params.json
    docstrings: !path
      path: ${env.TARGETFOLDER}/docstrings.json
    iteration_results: !path
      path: ${env.TARGETFOLDER}

  iteration_results: &iteration_results
    round: i
    actions: actions
    rewards: all_rewards
    parametrization: reward_distribution

  run: !python |
    params = algorithm.init_params(M)
  
    for i in range(int(round_count)):
        actions = algorithm.predict(l, params)
        all_rewards, actions_rewards, reward_distribution = bandit.run(i, actions)
        params = algorithm.update(params, actions, actions_rewards)
    
        observe(iteration_results)  

algorithm_docstring: &docstring !JsonQuery
  path: ${env.TARGETFOLDER}/docstrings.json

stage_params1: &stage_params !JsonQuery
  path: ${env.TARGETFOLDER}/params.json


prepare_data: &prepare_data !GenText
  items:
    - !NodeDecorator
      node: *common_params
      name: Experiment params
    - *sudden_bandit
    - *thompson_sampling
    - *trial_outputs
    - *stage_params
    - *trial_inputs
    - !NodeDecorator
      node: *iteration_results
      name: Observed attributes
  output_file: ${env.TARGETFOLDER}/generated_rdfs_with_text.csv


pipeline: !GenericPipeline
  runs:
    - *trial_stage
    - *prepare_data
