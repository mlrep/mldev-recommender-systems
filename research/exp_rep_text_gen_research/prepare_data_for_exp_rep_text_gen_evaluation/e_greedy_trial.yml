# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

common_params: &common_params
  M: 4
  l: 1


e_greedy: &e_greedy !Atom
  name: Epsilon-Greedy
  epsilon: 0.1
  <<: *common_params
  init_params: !function src/recommenders/bandit/e_greedy.init_parameters
  predict: !function src/recommenders/bandit/e_greedy.predict
  update: !function src/recommenders/bandit/e_greedy.update


sudden_bandit: &sudden_bandit !Atom
  name: Sudden Bandit
  <<: *common_params
  current_round: 0
  drift: ${env.DRIFT}
  abrupt_time_0: 0
  abrupt_time_1: 150
  abrupt_time_2: 300
  parametrization: [0.85, 0.85, 0.85, 0.9]
  parametrization_1: [0.01, 0.01, 0.01, 0.01]
  parametrization_2: [1.0, 0.0, 0.0, 0.0]
  sample_rewards: !function src/environments/bandit/drift/sudden.sample_rewards
  get_actions_reward: !function src/environments/bandit/drift/sudden.get_actions_reward
  run: !python-function |
      def run(current_round, actions):
          reward_distribution = parametrization
          if drift == 'True':
            if abrupt_time_0 <= current_round < abrupt_time_1:
              reward_distribution = parametrization_1
            elif abrupt_time_1 <= current_round < abrupt_time_2:
              reward_distribution = parametrization_2

          all_rewards = sample_rewards(reward_distribution, None, 0, 1)
          actions_rewards = get_actions_reward(actions, all_rewards)
    
          return all_rewards, actions_rewards, reward_distribution      
      return run


trial: &trial !TrialStage
  name: &trial_name non-stationarity-trial

  params:
    <<: *common_params
    trial: *trial_name
    bandit: *sudden_bandit
    algorithm: *e_greedy
    round_count: ${env.ROUNDCOUNT}

  inputs: &trial_inputs
    src: !path
      path: src
    config: !path
      path: ${env.EXPERIMENTFOLDER}/e_greedy_trial.yml

  outputs: &trial_outputs
    params: !path
      path: ${env.TARGETFOLDER}/params.json
    docstrings: !path
      path: ${env.TARGETFOLDER}/docstrings.json
    iteration_results: !path
      path: ${env.TARGETFOLDER}

  iteration_results: &iteration_results
    round: i
    actions: actions
    rewards: all_rewards
    parametrization: reward_distribution

  run: !python |
    action_rewards, action_choices_count, params = algorithm.init_params(M)

    for i in range(int(round_count)):
        actions = algorithm.predict(params, M, l, algorithm.epsilon)
        all_rewards, actions_rewards, reward_distribution = bandit.run(i, actions)
        action_rewards, action_choices_count, params = algorithm.update(
            action_rewards, action_choices_count, params, actions, actions_rewards)
    
        observe(iteration_results)

algorithm_docstring: &docstring !JsonQuery
  path: ${env.TARGETFOLDER}/docstrings.json

stage_params1: &stage_params !JsonQuery
  path: ${env.TARGETFOLDER}/params.json


prepare_data: &prepare_data !GenText
  items:
    - !NodeDecorator
      node: *common_params
      name: Experiment params
    - *sudden_bandit
    - *e_greedy
    - *trial_outputs
    - *stage_params
    - *trial_inputs
    - !NodeDecorator
      node: *iteration_results
      name: Observed attributes
  output_file: ${env.TARGETFOLDER}/generated_rdfs_with_text.csv


pipeline: !GenericPipeline
  runs:
    - *trial
    - *prepare_data


