======
Задача
======

Продолжение разведывательного анализа реальных данных для использования их в экспериментах
с многорукиим бандитами.

Продолжается поиск и исследование пользователей, события которых можно задействовать для перехода
к табличному определнию задачи многоруких бандитов.

+++++++++++++
Новые вводные
+++++++++++++

Основные результаты и выводы предыдущего исследования:

1. Пока без агрегации - одно событие - один раунд. Рассматривать только события корзины.
2. Кроме проблемы высокого разнообразия оказалось, что может быть и обратная проблема - пользователи совершают почти все события с одной категорией.

+++++++++++++++++++++++++++++++++++++
Цели и гипотезы текущего исследования
+++++++++++++++++++++++++++++++++++++

1. Просмотреть визуально примеры других пользователей, найденных по критериям аналогичным предыдущему исследованию. Проверить, насколько распространена ситуация, где пользователи совершают все покупки с одной категорией.
2. Возможно, обнаружить другие явления в логах событий пользователей.

==================
Метод исследования
==================

1. Используя статистики разнообразие категорий, число событий найти пользователей с большим числом событий и низким разнообразием.
2. Случайно отобрать пользователей для ручного анализа.
3. Построить графики и визуально выявить ситуации, где пользователи совершают все покупки с одной категорией.

====================
Процесс и результаты
====================

+++++++++++++++++++++++++++++++++
Выборка пользователей для анализа
+++++++++++++++++++++++++++++++++

Анализируются только события корзины и пользователи за один месяц (Октябрь 2019).

Рассчитанные статистики число событий и разнообразие категорий позволяют
выбирать пользователей для подробного анализа.

Предикат::

    predicate = (user_stats['category_diversity'] < 0.1)\
    & (user_stats['events_count'] > 100)


Отобрано пользователей: 187 из 133818

++++++++++++++++++++++++++++++++++++++++++++++++++
Визуальный анализ событий отобранных пользователей
++++++++++++++++++++++++++++++++++++++++++++++++++

Отберем случайно 14 пользователей и рассмотрим визуально лог событий.

.. figure:: {{ user_34033932_category_events_img }}
    :align: center

Интересы пользователя представлены преимущественно 3 категориями.
Вся активность умещается в 3 часа, это может быть не очень репрезентативный пример для исследования.

.. figure:: {{ user_187565158_category_events_img }}
    :align: center

По нескольким категориям много событий. Активность распределена по всему месяцу, но сессий
(моментов времени, вокруг которых можно сгруппировать события) не так много.

.. figure:: {{ user_202472817_category_events_img }}
    :align: center

Короткий временной интервал активности.

.. figure:: {{ user_230804370_category_events_img }}
    :align: center

Короткий временной интервал активности.

.. figure:: {{ user_230818889_category_events_img }}
    :align: center

Большинство событий принадлежат двум категориям.

.. figure:: {{ user_243173629_category_events_img }}
    :align: center

Всего три характерных сессии, неестественный пример.

.. figure:: {{ user_263366743_category_events_img }}
    :align: center

Более естественный пример, но снова преобладают две категории.

.. figure:: {{ user_276624000_category_events_img }}
    :align: center

Всего три характерных сессии.

.. figure:: {{ user_288864056_category_events_img }}
    :align: center

Похоже на одну сессию.

.. figure:: {{ user_300688877_category_events_img }}
    :align: center

Всего 4 сессии, преобладает одна категория.

.. figure:: {{ user_313751489_category_events_img }}
    :align: center

Преобладает две категории.

.. figure:: {{ user_338640563_category_events_img }}
    :align: center

Преобладает одна категория.

.. figure:: {{ user_355933481_category_events_img }}
    :align: center

Две сессии с двумя разными категориями.

.. figure:: {{ user_374479310_category_events_img }}
    :align: center

Более естественный пример.

======
Выводы
======

1. Ситуация, где пользователи совершают почти все события с одной категорией или с малым числом категорий встречается, не является редкой.
2. Также обнаружено, что нередкой является ситуация, когда все события пользователя сосредоточены вокруг малого числа моментов времени. У пользователей мало сессий.
3. Нередкой является ситуация, когда все события пользователя умещаются в несколько часов.

Хотя эти ситуации должны учитываться на практике, кажется, что выбирать для эксперимента лучше пользователей, которые:

1. Имеют несколько категорий интересов.
2. Их события рассосредоточены по времени и распределены по нескольким сессиям.

Следующим шагом предлагается формализовать понятие внутреннего разнообразия категорий - то,
насколько много у пользователя категорий с большим числом событий.

Оценить число таких пользователей и проанализировать визуально их логи событий.

