# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


import pandas as pd
import matplotlib.pyplot as plt


fig, ax = plt.subplots(3, 1, figsize=(9, 15))
fig.tight_layout(pad=5.0)

for i, l in enumerate((1, 2, 3)):
    ax[i].set_title(f'Expected Stochastic Regret vs Rounds for E-greedy with l={l}')
    ax[i].set_xlabel('Rounds')
    ax[i].set_ylabel('Regret')

    for M in ([5, 10, 15, 20]):
        base_path = f'results/e_greedy-vs-M/49/M:{M}_l:{l}/'
        df = pd.read_parquet(base_path + 'expected_stochastic_regret.parquet')

        ax[i].plot(df['expected_stochastic_regret'])
    ax[i].legend(['M = 5', 'M = 10', 'M = 15', 'M = 20'])

fig.savefig('results/e_greedy-vs-M/expected_stochastic_regret.png')