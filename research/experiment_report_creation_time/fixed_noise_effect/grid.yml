# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


trial_count: &trial_count 100
round_count: &round_count 1000
grid: &grid
  w: &w [ 0.0, 0.01, 0.1, 0.15, 0.2, 0.25, 0.5, 1.0 ]
  algorithm:
    - f_dsw_ts
    - thompson_sampling
    - exp3
    - e_greedy

trial_stages: &trial_stages {}
trial_factory: &trial_factory
  !BasicStageFactory
    stages_output: *trial_stages

    basename: "${env.EXPERIMENTNAME}"
    random_seed: "${env.RANDOMSEED}"
    trial_count: *trial_count
    iterable: !IterableGrid
      <<: *grid

    env:
      PYTHONPATH: '${env.PYTHONPATH}'
      TARGETFOLDER: "${path(self.outputs[0].path)}"
      PARAMSFILE: "${path(self.outputs[1])}"
      DOCSFILE: "${path(self.outputs[2])}"
      TRIALNUMBER: "${self.params.trial_number}"
      ROUNDCOUNT: *round_count

    outputs:
      - !path
        path: !line |
          ${env.TARGETFOLDER}/algorithm:${self.params.iterable.current.algorithm}/
          w:${self.params.iterable.current.w}/
          trial-num:${self.params.trial_number}
      - ${path(self.outputs[0].path)}/params.json
      - ${path(self.outputs[0].path)}/docstrings.json

    script:
      - ./venv/bin/mldev run -f research/experiment_report_creation_time/fixed_noise_effect/${self.params.iterable.current.algorithm}_trial.yml

# Указываем набор метрик
metric_stages: &metric_stages {}
metric_factories: &metric_factories

  - !StageFactory
    tag: ExpectedStochasticRegret
    stages_output: *metric_stages
    iterable: !IterableGrid
      <<: *grid
    params:
      data_path: !line |
        ${env.TARGETFOLDER}/algorithm:${self.params.iterable.current.algorithm}/
        w:${self.params.iterable.current.w}/
        trial-num:*/data-*.parquet
      result_path: !line |
        ${env.TARGETFOLDER}/algorithm:${self.params.iterable.current.algorithm}/
        w:${self.params.iterable.current.w}/
      columns_mapping:
        reward_distributions: !PythonExpression >
          lambda row: row['parametrization']
        selected_action_count: !PythonExpression >
          lambda row: len(row['actions'])
        selected_action_reward: !PythonExpression >
          lambda row: sum(row['rewards'][row['actions']])


f_dsw_ts_report: &f_dsw_ts_report !Report
  name: index
  template: research/experiment_report_creation_time/fixed_noise_effect/noise_template.rst
  output: results/fixed-noise-effect-experiment/f_dsw_ts_report/rst
  report_model:
    w: *w
    trial_count: *trial_count
    round_count: *round_count
    params: !JsonQuery
      path: results/fixed-noise-effect-experiment/49/algorithm:f_dsw_ts/w:0.0/trial-num:0/params.json
      query: '@'
    pictures:
      fig1:
        path: _static/f_dsw_ts_expected_stochastic_regret.png
        desc: Ожидаемый регрет в зависиоммости от раунда эксперимента.
      fig2:
        path: _static/f_dsw_ts_expected_stochastic_regret_100.png
        desc: Ожидаемый регрет в зависиоммости от раунда эксперимента, 980-1000 раунды, только :math:`w <= 0.2`


f_dsw_ts_sphinx: &f_dsw_ts_sphinx !BasicStage
  name: make_html
  script:
    - cp research/experiment_report_creation_time/fixed_noise_effect/conf.py results/fixed-noise-effect-experiment/f_dsw_ts_report/rst
    - mkdir -p results/fixed-noise-effect-experiment/f_dsw_ts_report/rst/_static
    - cp results/fixed-noise-effect-experiment/49/charts/f_dsw_ts_* results/fixed-noise-effect-experiment/f_dsw_ts_report/rst/_static/
    - cd results/fixed-noise-effect-experiment/f_dsw_ts_report
    - sphinx-build -b html rst html


thompson_sampling_report: &thompson_sampling_report !Report
  name: index
  template: research/experiment_report_creation_time/fixed_noise_effect/noise_template.rst
  output: results/fixed-noise-effect-experiment/thompson_sampling_report/rst
  report_model:
    w: *w
    trial_count: *trial_count
    round_count: *round_count
    params: !JsonQuery
      path: results/fixed-noise-effect-experiment/49/algorithm:thompson_sampling/w:0.0/trial-num:0/params.json
      query: '@'
    pictures:
      fig1:
        path: _static/thompson_sampling_expected_stochastic_regret.png
        desc: Ожидаемый регрет в зависиоммости от раунда эксперимента.
      fig2:
        path: _static/thompson_sampling_expected_stochastic_regret_100.png
        desc: Ожидаемый регрет в зависиоммости от раунда эксперимента, 980-1000 раунды, только :math:`w <= 0.2`


thompson_sampling_sphinx: &thompson_sampling_sphinx !BasicStage
  name: make_html
  script:
    - cp research/experiment_report_creation_time/fixed_noise_effect/conf.py results/fixed-noise-effect-experiment/thompson_sampling_report/rst
    - mkdir -p results/fixed-noise-effect-experiment/thompson_sampling_report/rst/_static
    - cp results/fixed-noise-effect-experiment/49/charts/thompson_sampling_* results/fixed-noise-effect-experiment/thompson_sampling_report/rst/_static/
    - cd results/fixed-noise-effect-experiment/thompson_sampling_report
    - sphinx-build -b html rst html


exp3_report: &exp3_report !Report
  name: index
  template: research/experiment_report_creation_time/fixed_noise_effect/noise_template.rst
  output: results/fixed-noise-effect-experiment/exp3_report/rst
  report_model:
    w: *w
    trial_count: *trial_count
    round_count: *round_count
    params: !JsonQuery
      path: results/fixed-noise-effect-experiment/49/algorithm:exp3/w:0.0/trial-num:0/params.json
      query: '@'
    pictures:
      fig1:
        path: _static/exp3_expected_stochastic_regret.png
        desc: Ожидаемый регрет в зависиоммости от раунда эксперимента.
      fig2:
        path: _static/exp3_expected_stochastic_regret_100.png
        desc: Ожидаемый регрет в зависиоммости от раунда эксперимента, 980-1000 раунды, только :math:`w <= 0.2`


exp3_sphinx: &exp3_sphinx !BasicStage
  name: make_html
  script:
    - cp research/experiment_report_creation_time/fixed_noise_effect/conf.py results/fixed-noise-effect-experiment/exp3_report/rst
    - mkdir -p results/fixed-noise-effect-experiment/exp3_report/rst/_static
    - cp results/fixed-noise-effect-experiment/49/charts/exp3_* results/fixed-noise-effect-experiment/exp3_report/rst/_static/
    - cd results/fixed-noise-effect-experiment/exp3_report
    - sphinx-build -b html rst html


e_greedy_report: &e_greedy_report !Report
  name: index
  template: research/experiment_report_creation_time/fixed_noise_effect/noise_template.rst
  output: results/fixed-noise-effect-experiment/e_greedy_report/rst
  report_model:
    w: *w
    trial_count: *trial_count
    round_count: *round_count
    params: !JsonQuery
      path: results/fixed-noise-effect-experiment/49/algorithm:e_greedy/w:0.0/trial-num:0/params.json
      query: '@'
    pictures:
      fig1:
        path: _static/e_greedy_expected_stochastic_regret.png
        desc: Ожидаемый регрет в зависиоммости от раунда эксперимента.
      fig2:
        path: _static/e_greedy_expected_stochastic_regret_100.png
        desc: Ожидаемый регрет в зависиоммости от раунда эксперимента, 980-1000 раунды, только :math:`w <= 0.2`


e_greedy_sphinx: &e_greedy_sphinx !BasicStage
  name: make_html
  script:
    - cp research/experiment_report_creation_time/fixed_noise_effect/conf.py results/fixed-noise-effect-experiment/e_greedy_report/rst
    - mkdir -p results/fixed-noise-effect-experiment/e_greedy_report/rst/_static
    - cp results/fixed-noise-effect-experiment/49/charts/e_greedy_* results/fixed-noise-effect-experiment/e_greedy_report/rst/_static/
    - cd results/fixed-noise-effect-experiment/e_greedy_report
    - sphinx-build -b html rst html


pipeline: !GenericPipeline
  runs: !flatten
    # Запуск триалов
    - *trial_factory
    - *trial_stages

    # Расчет метрик
    - *metric_factories
    - *metric_stages

    # Создание пустой директории для сохранения изображений
    - !BasicStage
      name: mkdir
      script:
        - mkdir -p ${env.TARGETFOLDER}/charts

    # Визуализация
    - !JupyterStage
        name: visualization
        notebook_pipeline: research/experiment_report_creation_time/fixed_noise_effect/draw_graphs.pipeline

    # Генерация отчетов
    - *f_dsw_ts_report
    - *f_dsw_ts_sphinx
    - *thompson_sampling_report
    - *thompson_sampling_sphinx
    - *exp3_report
    - *exp3_sphinx
    - *e_greedy_report
    - *e_greedy_sphinx
