# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

common_params: &common_params
  M: 4
  l: 1


f_dsw_ts: &f_dsw_ts !Atom
  name: f-dsw TS
  <<: *common_params
  window_size: 50
  gamma: 0.999
  init_params: !function src/recommenders/bandit/f_dsw_ts.init_parameters
  predict: !function src/recommenders/bandit/f_dsw_ts.predict
  update: !function src/recommenders/bandit/f_dsw_ts.update


sudden_bandit: &sudden_bandit !Atom
  name: Sudden Bandit
  <<: *common_params
  current_round: 0
  drift: ${env.DRIFT}
  abrupt_time_0: 0
  abrupt_time_1: 150
  abrupt_time_2: 300
  parametrization: [0.85, 0.85, 0.85, 0.9]
  parametrization_1: [0.01, 0.01, 0.01, 0.01]
  parametrization_2: [1.0, 0.0, 0.0, 0.0]
  sample_rewards: !function src/environments/bandit/drift/sudden.sample_rewards
  get_actions_reward: !function src/environments/bandit/drift/sudden.get_actions_reward
  run: !python-function |
      def run(current_round, actions):
          reward_distribution = parametrization
          if drift == 'True':
            if abrupt_time_0 <= current_round < abrupt_time_1:
              reward_distribution = parametrization_1
            elif abrupt_time_1 <= current_round < abrupt_time_2:
              reward_distribution = parametrization_2

          all_rewards = sample_rewards(reward_distribution, None, 0, 1)
          actions_rewards = get_actions_reward(actions, all_rewards)
    
          return all_rewards, actions_rewards, reward_distribution      
      return run


pipeline: !GenericPipeline
  runs:
    - !TrialStage
      name: &trial_name non-stationarity-trial

      params:
        <<: *common_params
        trial: *trial_name
        bandit: *sudden_bandit
        algorithm: *f_dsw_ts
        round_count: ${env.ROUNDCOUNT}

      inputs:
        src: !path
          path: src
        config: !path
          path: research/experiment_report_creation_time/non_stationarity_analysis/e_greedy_trial.yml

      outputs:
        params: !path
          path: ${env.TARGETFOLDER}/params.json
        docstrings: !path
          path: ${env.TARGETFOLDER}/docstrings.json
        iteration_results: !path
          path: ${env.TARGETFOLDER}

      iteration_results:
        round: i
        actions: actions
        rewards: all_rewards
        parametrization: reward_distribution

      run: !python |
        historic_trace_params, hot_trace_params, last_rewards_per_action = algorithm.init_params(M)
        
        for i in range(int(round_count)):
            actions = algorithm.predict(historic_trace_params, hot_trace_params, M, l)
            all_rewards, actions_rewards, reward_distribution = bandit.run(i, actions)
            historic_trace_params, hot_trace_params, last_rewards_per_action = algorithm.update(
                actions,
                actions_rewards,
                hot_trace_params,
                historic_trace_params,
                last_rewards_per_action,
                algorithm.gamma,
                algorithm.window_size,
            )        

            observe(iteration_results)
