# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


trial_count: &trial_count 10
round_count: &round_count 25000
grid: &grid
  drift: [ on, off ]
  algorithm:
    - f_dsw_ts
    - thompson_sampling
    - exp3
    - e_greedy

trial_stages: &trial_stages {}
trial_factory: &trial_factory
  !BasicStageFactory
    stages_output: *trial_stages

    basename: "${env.EXPERIMENTNAME}"
    random_seed: "${env.RANDOMSEED}"
    trial_count: *trial_count
    iterable: !IterableGrid
      <<: *grid

    env:
      PYTHONPATH: '${env.PYTHONPATH}'
      TARGETFOLDER: "${path(self.outputs[0].path)}"
      PARAMSFILE: "${path(self.outputs[1])}"
      DOCSFILE: "${path(self.outputs[2])}"
      TRIALNUMBER: "${self.params.trial_number}"
      ROUNDCOUNT: *round_count

    outputs:
      - !path
        path: !line |
          ${env.TARGETFOLDER}/algorithm:${self.params.iterable.current.algorithm}/
          drift:${self.params.iterable.current.drift}/
          trial-num:${self.params.trial_number}
      - ${path(self.outputs[0].path)}/params.json
      - ${path(self.outputs[0].path)}/docstrings.json

    script:
      - ./venv/bin/mldev run -f research/experiment_report_creation_time/non_stationarity_analysis/${self.params.iterable.current.algorithm}_trial.yml

# Указываем набор метрик
metric_stages: &metric_stages {}
metric_factories: &metric_factories

  - !StageFactory
    tag: ExpectedStochasticRegret
    stages_output: *metric_stages
    iterable: !IterableGrid
      <<: *grid
    params:
      data_path: !line |
        ${env.TARGETFOLDER}/algorithm:${self.params.iterable.current.algorithm}/
        drift:${self.params.iterable.current.drift}/
        trial-num:*/data-*.parquet
      result_path: !line |
        ${env.TARGETFOLDER}/algorithm:${self.params.iterable.current.algorithm}/
        drift:${self.params.iterable.current.drift}/
      columns_mapping:
        reward_distributions: !PythonExpression >
          lambda row: row['parametrization']
        selected_action_count: !PythonExpression >
          lambda row: len(row['actions'])
        selected_action_reward: !PythonExpression >
          lambda row: sum(row['rewards'][row['actions']])


pipeline: !GenericPipeline
  runs: !flatten
    # Запуск триалов
    - *trial_factory
    - *trial_stages

    # Расчет метрик
    - *metric_factories
    - *metric_stages

    # Создание пустой директории для сохранения изображений
    - !BasicStage
      name: mkdir
      script:
        - mkdir -p ${env.TARGETFOLDER}/charts

    # Визуализация
    - !JupyterStage
        name: visualization
        notebook_pipeline: research/experiment_report_creation_time/non_stationarity_analysis/draw_graphs.pipeline
