# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

common_params: &common_params
  M: 10
  l: 1


# Algorithm
exp3_algorithm: &exp3_algorithm !Atom
  name: exp3_algorithm
  <<: *common_params
  init_params: !function src/recommenders/bandit/exp3.init_parameters
  predict: !function src/recommenders/bandit/exp3.predict
  get_distribution: !function src/recommenders/bandit/exp3.get_distribution
  update: !function src/recommenders/bandit/exp3.update_weights
  gamma: 0.07


# Bandit
bernoulli_bandit: &bernoulli_bandit !Atom
  name: bernoulli_bandit
  <<: *common_params
  parametrization: [0.5, 0.33, 0.25, 0.2, 0.17, 0.14, 0.125, 0.11, 0.1, 0.09]
  sample_rewards: !function src/environments/bandit/stationary/bernoulli.sample_rewards
  get_actions_reward: !function src/environments/bandit/stationary/bernoulli.get_actions_reward
  response: !python-function |
      def response(actions):
          rewards = sample_rewards(parametrization)
          actions_reward = get_actions_reward(actions, rewards)
          return rewards, actions_reward
    
      return response


exp3_verification: &exp3_verification !TrialStage
  name: ${env.EXPERIMENTNAME}
  description:
    "Test robustness of Exp3 algorithm."

  params:
    trial: "${env.EXPERIMENTNAME}"
    round_count: 10000
    bandit: *bernoulli_bandit
    algorithm: *exp3_algorithm
    <<: *common_params

  inputs:
    src: !path
      path: src
    config: !path
      path: tests/verification/exp3/trial.yml

  outputs:
    params: !path
      path: ${env.TARGETFOLDER}/params.json
    docstrings: !path
      path: ${env.TARGETFOLDER}/docstrings.json
    iteration_results: !path
      path: ${env.TARGETFOLDER}

  iteration_results:
    round: i
    actions: actions
    rewards: rewards
    parametrization: bandit.parametrization

  run: !python |
      weights = algorithm.init_params(M)
      gamma = algorithm.gamma 
    
      for i in range(round_count):
          distribution = algorithm.get_distribution(weights, gamma, M)
          actions = algorithm.predict(M, distribution)    
          rewards, actions_reward = bandit.response(actions)
          weights = algorithm.update(
              weights, actions_reward, distribution, actions, gamma, M)

          observe(iteration_results)


pipeline: !GenericPipeline
  runs:
    - *exp3_verification
