# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

common_params: &common_params
  M: 2
  l: 1


# Algorithm
exp3_algorithm: &exp3_algorithm !Atom
  name: exp3_algorithm
  <<: *common_params
  gamma: 0.01
  init_params: !function src/recommenders/bandit/exp3.init_parameters
  predict: !function src/recommenders/bandit/exp3.predict
  get_distribution: !function src/recommenders/bandit/exp3.get_distribution
  update: !function src/recommenders/bandit/exp3.update_weights


# User Model
bernoulli_bandit: &bernoulli_bandit !Atom
  name: bernoulli_bandit
  <<: *common_params
  parametrization: [0.5, 0.6]
  sample_rewards: !function src/environments/bandit/stationary/bernoulli.sample_rewards
  get_actions_reward: !function src/environments/bandit/stationary/bernoulli.get_actions_reward
  response: !python-function |
      def response(actions):
          all_rewards = sample_rewards(parametrization)
          return get_actions_reward(actions, all_rewards)

      return response


exp3_verification: &exp3_verification !TrialStage
  name: ${env.EXPERIMENTNAME}
  description:
    "Test robustness of Exp3 algorithm relative to concept drift user model (sudden case)."

  params:
    <<: *common_params
    trial: "${env.EXPERIMENTNAME}"
    round_count: 10000
    bandit: *bernoulli_bandit
    algorithm: *exp3_algorithm

  inputs:
    src: !path
      path: src
    config: !path
      path: ${env.SOURCECODEFOLDER}/trial.yml

  outputs:
    iteration_results: !path
      path: ${env.TARGETFOLDER}

  iteration_results:
    round: i
    rewards: rewards
    actions: actions

  run: !python |
      weights = algorithm.init_params(M)

      for i in range(round_count):
          distribution = algorithm.get_distribution(weights, algorithm.gamma, M)
          actions = algorithm.predict(M, distribution)
          rewards = bandit.response(actions)        
          weights = algorithm.update(
              weights, rewards, distribution, actions, algorithm.gamma, M)

          observe(iteration_results)


pipeline: !GenericPipeline
  runs:
    - *exp3_verification
