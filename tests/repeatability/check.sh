#!/bin/bash
# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


RUN_COUNT=50
for i in $(seq 1 $RUN_COUNT)
do
  RUNNUMBER=$i mldev run -f tests/repeatability/experiment.yml >/dev/null 2>&1
done

if test "$( ls -1 results/repeatability-experiment/run*/*/*.parquet | xargs sha256sum |  awk '{print $1}' | wc -l )" -eq "$RUN_COUNT"
then
  if test "$( ls -1 results/repeatability-experiment/run*/*/*.parquet | xargs sha256sum |  awk '{print $1}' | uniq -c | wc -l )" -eq "1"
  then
    echo 'OK'
  else
    echo 'FAILED'
  fi
else
  echo 'FAILED'
fi
