# Тестирование инвариантами стационарных алгоритмов многоруких бандитов

В данной папке представлены реализации 12 различных инвариантов.

Для запуска эксперимента с любым из инвраиантов достаточно указать в конфиге experiments.yml название инварианта.


### test_stationary_algorithms и test_context_algorithms_on_stationary_context

#### 1: same_parameters_lead_to_same_outcomes

При запуске с одними и теми же параметрами алгоритм будет работать одинаково.

#### 2: reward_in_the_beginning_is_less_than_reward_in_the_end

Если алгоритм обучается, то средняя награда в начале обучения должна быть не меньше средней награды в конце.

#### 3: presence_of_trend

Если алгоритм обучается, должен присутствовать возрастающий тренд средней награды.

#### 4: monotonic_increase_of_reward

Если алгоритм обучается, средняя награда будет монотонно возрастать.

#### 5: greater_reward_probabilities_lead_to_greater_rewards

Если вероятность получить награду увеличивается, средняя награда увеличивается.

#### 6: multiplied_rewards_probabilities_lead_to_greater_rewards

Если умножить вероятности получения награды на одно и то же число больше 1, то средняя награда увеличится.

#### 7: add_constant_to_all_probabilities_lead_to_greater_rewards

Если умножить вероятности получения награды на одно и то же число больше 1, то средняя награда увеличится.

#### 8: shuffled_parameters_lead_to_same_outcomes

При запуске алгоритма с фиксированными параметрами на одной и той же конфигурации бандита с точностью до перестановки ручек местами средняя награда должна быть одной и той же.

### test_stationary_algorithms_on_drift и test_context_algorithms_on_stationary_context_and_interest_drift

#### 9 sudden_drift_leads_to_reward_decrease

Если предпочтения бандита резко изменяются, средняя награда уменьшается.

#### 10 reward_after_drift_is_less_than_reward_in_the_end

После резкого изменения предпочтений без дальнейших изменений средняя награда должна возрастать. 

### test_context_algorithms_on_context_drift_and_stationary_interest

#### 11 sudden_drift_leads_to_reward_decrease

Если при неизменных предпочтениях бандита резко изменяется контекст, средняя награда уменьшается.

#### 12 reward_after_drift_is_less_than_reward_in_the_end

После резкого изменения контекста без дальнейших изменений средняя награда должна возрастать.