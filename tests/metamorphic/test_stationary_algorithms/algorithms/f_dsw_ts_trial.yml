# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

common_params: &common_params
  M: 10
  l: 3


f_dsw_ts: &f_dsw_ts !Atom
  name: f-dsw TS
  <<: *common_params
  window_size: 50
  gamma: 0.999
  init_params: !function src/recommenders/bandit/f_dsw_ts.init_parameters
  predict: !function src/recommenders/bandit/f_dsw_ts.predict
  update: !function src/recommenders/bandit/f_dsw_ts.update


bernoulli_bandit: &bernoulli_bandit !Atom
  name: Bernoulli Bandit
  <<: *common_params
  parametrization: !JsonLoader ${env.GENERATEDPARAMETERSFILE}
  sample_rewards: !function src/environments/bandit/stationary/bernoulli.sample_rewards

pipeline: !GenericPipeline
  runs:
    - !TrialStage
      name: &trial_name fixed-noise-effect-f-dsw-ts

      params:
        <<: *common_params
        trial: *trial_name
        bandit: *bernoulli_bandit
        algorithm: *f_dsw_ts
        round_count: ${env.ROUNDCOUNT}

      inputs:
        src: !path
          path: src
        config: !path
          path: ${env.SOURCECODEFOLDER}/trial2.yml

      outputs:
        params: !path
          path: ${env.TARGETFOLDER}/params.json
        docstrings: !path
          path: ${env.TARGETFOLDER}/docstrings.json
        iteration_results: !path
          path: ${env.TARGETFOLDER}

      iteration_results:
        round: i
        actions: actions
        rewards: all_rewards
        mean_reward: mean_reward

      run: !python |
        historic_trace_params, hot_trace_params, last_rewards_per_action = algorithm.init_params(M)
        
        for i in range(int(round_count)):
            actions = algorithm.predict(historic_trace_params, hot_trace_params, M, l)
            all_rewards = bandit.sample_rewards(bandit.parametrization["parametrization"])
            historic_trace_params, hot_trace_params, last_rewards_per_action = algorithm.update(
                actions,
                all_rewards,
                hot_trace_params,
                historic_trace_params,
                last_rewards_per_action,
                algorithm.gamma,
                algorithm.window_size,
            )        

            mean_reward = sum(all_rewards[actions.tolist()])/len(actions.tolist())
            observe(iteration_results)
