import numpy as np
import pytest

from environments.bandit.adversarial.record_replay import init, get_actions_reward


SUCCESS_PARAMS_LIST = [
    [10, [1, 2, 3], 5, [0] * 10],
    [5, [1, 5], 1, [0, 1, 0, 0, 0]],
]

FAIL_PARAMS_LIST = [
    [8, [6, 9], 9, False],
    [8, [6, 9], 7, False],
]


def test_init_record_returns_corret_record():
    record, encoder = init(
        data_path='tests/unit/environments/test_data.csv',
        user_id=2000558,
        action_column='PRODUCT_SUBCLASS',
        user_column='CUSTOMER_ID',
    )

    record_length = len(record)

    assert record_length > 0
    assert isinstance(record[0], np.int32)


@pytest.mark.parametrize('success_params', SUCCESS_PARAMS_LIST)
def test_get_actions_success_given_bool_input(success_params):
    reward = get_actions_reward(*success_params[:-1])
    assert np.allclose(reward, np.array(success_params[-1]))


@pytest.mark.parametrize('fail_params', FAIL_PARAMS_LIST)
def test_get_actions_fails_given_bool_input(fail_params):
    with pytest.raises(Exception):
        get_actions_reward(fail_params)
