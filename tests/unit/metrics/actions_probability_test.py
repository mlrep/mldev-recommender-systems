import numpy as np
import pandas as pd
import unittest

from metrics import ActionsProbability


class TestActionsProbabilityMetric(unittest.TestCase):

    def test_metric(self):
        trials = [
            pd.DataFrame(
                [
                    [1, np.array([0, 1, 0])],
                    [2, np.array([0, 0, 1])],
                    [3, np.array([0, 0, 0])],
                    [4, np.array([1, 1, 1])],
                ],
                columns=['round', 'actions_vector']
            ),
            pd.DataFrame(
                [
                    [1, np.array([0, 1, 1])],
                    [2, np.array([1, 1, 0])],
                    [3, np.array([0, 0, 0])],
                    [4, np.array([1, 1, 1])],
                ],
                columns=['round', 'actions_vector']
            ),
            pd.DataFrame(
                [
                    [1, np.array([0, 0, 0])],
                    [2, np.array([0, 0, 0])],
                    [3, np.array([0, 0, 0])],
                    [4, np.array([1, 1, 1])],
                ],
                columns=['round', 'actions_vector']
            ),
        ]
        expected_result = pd.DataFrame(
            [
                [1, np.array([0., 2/3, 1/3])],
                [2, np.array([1/3, 1/3, 1/3])],
                [3, np.array([0., 0., 0.])],
                [4, np.array([1., 1., 1.])],
            ],
            columns=['round', ActionsProbability.metric_name]
        ).set_index('round')

        result = ActionsProbability.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))


if __name__ == '__main__':
    unittest.main()
