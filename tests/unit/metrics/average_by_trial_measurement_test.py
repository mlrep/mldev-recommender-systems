import pandas as pd
import unittest

from metrics import AverageByTrialMeasurement


class TestAverageByTrialMeasurementMetric(unittest.TestCase):

    def test_similar_values_in_trials(self):
        df = pd.DataFrame(
            [
                [1, 0.],
                [2, 1.],
                [3, 0.],
                [4, 2.],
                [5, 1.],
            ],
            columns=['round', 'measure']
        )
        trials = [df, df.copy(), df.copy()]

        metric = AverageByTrialMeasurement.metric_name
        expected_result = pd.DataFrame(
            [
                [1, 0., (None, None)],
                [2, 1., (None, None)],
                [3, 0., (None, None)],
                [4, 2., (None, None)],
                [5, 1., (None, None)],
            ],
            columns=['round', metric, f'{metric}_confidence_interval']
        ).set_index('round')

        result = AverageByTrialMeasurement.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))

    def test_one_trial(self):
        trials = [
            pd.DataFrame(
                [
                    [1, 0.],
                    [2, 1.],
                    [3, 0.],
                    [4, 2.],
                    [5, 1.],
                ],
                columns=['round', 'measure']
            )
        ]

        metric = AverageByTrialMeasurement.metric_name
        expected_result = pd.DataFrame(
            [
                [1, 0., (None, None)],
                [2, 1., (None, None)],
                [3, 0., (None, None)],
                [4, 2., (None, None)],
                [5, 1., (None, None)],
            ],
            columns=['round', metric, f'{metric}_confidence_interval']
        ).set_index('round')

        result = AverageByTrialMeasurement.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))

    def test_few_trials(self):
        trials = [
            pd.DataFrame(
                [
                    [1, 0.],
                    [2, 1.],
                    [3, 0.],
                    [4, 1.5],
                    [5, 1.],
                ],
                columns=['round', 'measure']
            ),
            pd.DataFrame(
                [
                    [1, 2.],
                    [2, 4.],
                    [3, 3.],
                    [4, 1.],
                    [5, 5.],
                ],
                columns=['round', 'measure']
            ),
            pd.DataFrame(
                [
                    [1, 4.],
                    [2, 4.],
                    [3, 0.],
                    [4, 5.],
                    [5, 0.],
                ],
                columns=['round', 'measure']
            )
        ]

        metric = AverageByTrialMeasurement.metric_name
        expected_result = pd.DataFrame(
            [
                [1, 2],
                [2, 3.],
                [3, 1.],
                [4, 2.5],
                [5, 2.],
            ],
            columns=['round', metric]
        ).set_index('round')

        result = AverageByTrialMeasurement.calculate(trials)
        self.assertTrue(expected_result[metric].equals(result[0][metric]))


if __name__ == '__main__':
    unittest.main()
