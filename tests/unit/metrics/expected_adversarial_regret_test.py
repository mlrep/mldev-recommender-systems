import numpy as np
import pandas as pd
import unittest

from metrics import ExpectedAdversarialRegret


class TestExpectedAdversarialRegretMetric(unittest.TestCase):

    def test_one_actions(self):
        df = pd.DataFrame(
            [
                [1, np.array([0]), np.array([0.1, 0.3, 0.5])],
                [2, np.array([2]), np.array([0.1, 0.3, 0.5])],
                [3, np.array([2]), np.array([0.5, 0.3, 0.1])],
                [4, np.array([1]), np.array([0.5, 0.3, 0.3])],
                [5, np.array([1]), np.array([0.5, 0.3, 0.2])],
            ],
            columns=['round', 'selected_action_indexes', 'all_action_rewards']
        )
        trials = [df, df.copy(), df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0.],  # 0.1 - 0.1
                [2, -0.4],  # 0.1 - 0.5
                [3, 0.0],  # 0.5 - 0.1
                [4, 0.2],  # 0.5 - 0.3
                [5, 0.4],  # 0.5 - 0.3
            ],
            columns=['round', ExpectedAdversarialRegret.metric_name]
        ).set_index('round')

        result = ExpectedAdversarialRegret.calculate(trials)
        self.assertTrue(
            (result[0] - expected_result).round(1).sum()[ExpectedAdversarialRegret.metric_name] == 0.
        )

    def test_multiple_actions(self):
        df = pd.DataFrame(
            [
                [1, np.array([0, 1]), np.array([0.1, 0.3, 0.5])],
                [2, np.array([1, 2]), np.array([0.1, 0.3, 0.5])],
                [3, np.array([0, 2]), np.array([0.5, 0.3, 0.1])],
                [4, np.array([0, 1]), np.array([0.5, 0.3, 0.3])],
                [5, np.array([0, 1]), np.array([0.5, 0.3, 0.2])],
            ],
            columns=['round', 'selected_action_indexes', 'all_action_rewards']
        )
        trials = [df, df.copy(), df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0.2],  # 0.6 - 0.4
                [2, 0.],  # 0.6 - 0.8
                [3, 0.],  # 0.6 - 0.6
                [4, 0.],  # 0.8 - 0.8
                [5, -0.1],  # 0.7 - 0.8
            ],
            columns=['round', ExpectedAdversarialRegret.metric_name]
        ).set_index('round')

        result = ExpectedAdversarialRegret.calculate(trials)
        self.assertTrue(
            (result[0] - expected_result).round(1).sum()[ExpectedAdversarialRegret.metric_name] == 0.
        )


if __name__ == '__main__':
    unittest.main()
