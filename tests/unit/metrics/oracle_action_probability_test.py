import numpy as np
import pandas as pd
import unittest

from metrics import OracleActionProbability


class TestOracleActionProbabilityMetric(unittest.TestCase):

    def test_metric(self):
        df = pd.DataFrame(
            [
                [1, np.array([0]), np.array([0.1, 0.3, 0.5])],
                [2, np.array([1]), np.array([0.1, 0.3, 0.5])],
                [3, np.array([0]), np.array([0.5, 0.3, 0.1])],
                [4, np.array([2]), np.array([0.1, 0.3, 0.5])],
                [5, np.array([1]), np.array([0.1, 0.3, 0.5])],
            ],
            columns=['round', 'selected_action_indexes', 'all_action_rewards']
        )
        trials = [df, df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0.],
                [2, 0.],
                [3, 1.],
                [4, 1.],
                [5, 0.],
            ],
            columns=['round', OracleActionProbability.metric_name]
        ).set_index('round')

        result = OracleActionProbability.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))


if __name__ == '__main__':
    unittest.main()
