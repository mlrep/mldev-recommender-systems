import numpy as np
import pandas as pd
import unittest

from metrics import CumulativeOracleActionPercentage


class TestCumulativeOracleActionPercentageMetric(unittest.TestCase):

    def test_one_action(self):
        df = pd.DataFrame(
            [
                [1, np.array([0]), np.array([0.1, 0.3, 0.5])],
                [2, np.array([1]), np.array([0.1, 0.3, 0.5])],
                [3, np.array([0]), np.array([0.5, 0.3, 0.1])],
                [4, np.array([1]), np.array([0.1, 0.3, 0.5])],
                [5, np.array([1]), np.array([0.1, 0.3, 0.5])],
            ],
            columns=['round', 'selected_action_indexes', 'all_action_rewards']
        )
        trials = [df, df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0],
                [2, 0],
                [3, 1/3],
                [4, 1/4],
                [5, 1/5],
            ],
            columns=['round', CumulativeOracleActionPercentage.metric_name]
        ).set_index('round')

        result = CumulativeOracleActionPercentage.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))

    def test_multiple_actions(self):
        df = pd.DataFrame(
            [
                [1, np.array([0, 1]), np.array([0.1, 0.3, 0.5])],
                [2, np.array([1, 0]), np.array([0.1, 0.3, 0.5])],
                [3, np.array([0, 2]), np.array([0.1, 0.3, 0.5])],
                [4, np.array([0, 1]), np.array([0.5, 0.3, 0.1])],
                [5, np.array([1, 2]), np.array([0.1, 0.3, 0.5])],
                [6, np.array([1, 2]), np.array([0.1, 0.3, 0.5])],
            ],
            columns=['round', 'selected_action_indexes', 'all_action_rewards']
        )
        trials = [df, df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0],
                [2, 0],
                [3, 0],
                [4, 1/4],
                [5, 2/5],
                [6, 3/6],
            ],
            columns=['round', CumulativeOracleActionPercentage.metric_name]
        ).set_index('round')

        result = CumulativeOracleActionPercentage.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))

    def test_all_actions(self):
        df = pd.DataFrame(
            [
                [1, np.array([0, 1, 2]), np.array([0.1, 0.3, 0.5])],
                [2, np.array([2, 1, 0]), np.array([0.1, 0.3, 0.5])],
                [3, np.array([0, 1, 2]), np.array([0.5, 0.3, 0.1])],
                [4, np.array([0, 1, 2]), np.array([0.1, 0.3, 0.5])],
                [5, np.array([0, 1, 2]), np.array([0.1, 0.3, 0.5])],
            ],
            columns=['round', 'selected_action_indexes', 'all_action_rewards']
        )
        trials = [df, df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 1.],
                [2, 1.],
                [3, 1.],
                [4, 1.],
                [5, 1.],
            ],
            columns=['round', CumulativeOracleActionPercentage.metric_name]
        ).set_index('round')

        result = CumulativeOracleActionPercentage.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))

    def test_zero_percentage(self):
        df = pd.DataFrame(
            [
                [1, np.array([0, 1]), np.array([0.1, 0.3, 0.5])],
                [2, np.array([1, 0]), np.array([0.1, 0.3, 0.5])],
                [3, np.array([0, 2]), np.array([0.5, 0.3, 0.1])],
                [4, np.array([0, 1]), np.array([0.1, 0.3, 0.5])],
                [5, np.array([1, 0]), np.array([0.1, 0.3, 0.5])],
            ],
            columns=['round', 'selected_action_indexes', 'all_action_rewards']
        )
        trials = [df, df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0.],
                [2, 0.],
                [3, 0.],
                [4, 0.],
                [5, 0.],
            ],
            columns=['round', CumulativeOracleActionPercentage.metric_name]
        ).set_index('round')

        result = CumulativeOracleActionPercentage.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))

    def test_one_round(self):
        df = pd.DataFrame(
            [
                [1, np.array([1, 2]), np.array([0.1, 0.3, 0.5])],
            ],
            columns=['round', 'selected_action_indexes', 'all_action_rewards']
        )
        trials = [df, df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 1.],
            ],
            columns=['round', CumulativeOracleActionPercentage.metric_name]
        ).set_index('round')

        result = CumulativeOracleActionPercentage.calculate(trials)
        self.assertTrue(expected_result.equals(result[0]))


if __name__ == '__main__':
    unittest.main()
