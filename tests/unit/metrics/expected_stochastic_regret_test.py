import numpy as np
import pandas as pd
import unittest

from metrics import ExpectedStochasticRegret


class TestExpectedStochasticRegretMetric(unittest.TestCase):

    def test_one_actions(self):
        df = pd.DataFrame(
            [
                [1, 1, [0.1, 0.3, 0.5], 0.3],
                [2, 1, [0.1, 0.3, 0.5], 0.5],
                [3, 1, [0.1, 0.3, 0.5], 0.1],
                [4, 1, [0.1, 0.3, 0.5], 0.1],
                [5, 1, [0.1, 0.3, 0.5], 0.3],
            ],
            columns=['round', 'selected_action_count', 'reward_distributions', 'selected_action_reward']
        )
        trials = [df, df.copy(), df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0.2],  # 0.5 - 0.3
                [2, 0.2],  # 0.5 - 0.5
                [3, 0.6],  # 0.5 - 0.1
                [4, 1.],  # 0.5 - 0.1
                [5, 1.2],  # 0.5 - 0.3
            ],
            columns=['round', ExpectedStochasticRegret.metric_name]
        ).set_index('round')

        result = ExpectedStochasticRegret.calculate(trials)
        self.assertTrue(
            (result[0] - expected_result).round(1).sum()[ExpectedStochasticRegret.metric_name] == 0.
        )

    def test_multiple_actions(self):
        df = pd.DataFrame(
            [
                [1, 2, [0.1, 0.3, 0.5], 0.6],
                [2, 2, [0.1, 0.3, 0.5], 0.8],
                [3, 2, [0.1, 0.3, 0.5], 0.4],
                [4, 2, [0.1, 0.3, 0.5], 0.6],
                [5, 2, [0.1, 0.3, 0.5], 0.4],
            ],
            columns=['round', 'selected_action_count', 'reward_distributions', 'selected_action_reward']
        )
        trials = [df, df.copy(), df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0.2],  # 0.8 - 0.6
                [2, 0.2],  # 0.8 - 0.8
                [3, 0.6],  # 0.8 - 0.4
                [4, 0.8],  # 0.8 - 0.6
                [5, 1.2],  # 0.8 - 0.4
            ],
            columns=['round', ExpectedStochasticRegret.metric_name]
        ).set_index('round')

        result = ExpectedStochasticRegret.calculate(trials)
        self.assertTrue(
            (result[0] - expected_result).round(1).sum()[ExpectedStochasticRegret.metric_name] == 0.
        )

    def test_non_stationary(self):
        df = pd.DataFrame(
            [
                [1, 1, [0.1, 0.3, 0.5], 0.3],
                [2, 1, [0.1, 0.5, 0.1], 0.5],
                [3, 1, [0.1, 0.8, 0.8], 0.1],
                [4, 1, [0.1, 0.3, 0.6], 0.1],
                [5, 1, [0.4, 0.3, 0.5], 0.3],
            ],
            columns=['round', 'selected_action_count', 'reward_distributions', 'selected_action_reward']
        )
        trials = [df, df.copy(), df.copy()]
        expected_result = pd.DataFrame(
            [
                [1, 0.2],  # 0.5 - 0.3
                [2, 0.2],  # 0.5 - 0.5
                [3, 0.9],  # 0.8 - 0.1
                [4, 1.4],  # 0.6 - 0.1
                [5, 1.6],  # 0.5 - 0.3
            ],
            columns=['round', ExpectedStochasticRegret.metric_name]
        ).set_index('round')

        result = ExpectedStochasticRegret.calculate(trials)
        self.assertTrue(
            (result[0] - expected_result).round(1).sum()[ExpectedStochasticRegret.metric_name] == 0.
        )


if __name__ == '__main__':
    unittest.main()
