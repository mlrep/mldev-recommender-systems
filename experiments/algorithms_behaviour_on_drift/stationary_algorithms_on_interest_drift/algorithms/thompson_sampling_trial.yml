# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

common_params: &common_params
  M: 5
  l: 2


thompson_sampling_algorithm: &thompson_sampling !Atom
  name: Thompson Sampling
  <<: *common_params
  init_params: !function src/recommenders/bandit/thompson_sampling.init_parameters
  predict: !function src/recommenders/bandit/thompson_sampling.predict
  update: !function src/recommenders/bandit/thompson_sampling.update


bernoulli_bandit: &bernoulli_bandit !Atom
  name: Bernoulli Bandit
  <<: *common_params
  parametrization: !JsonLoader ${env.GENERATEDPARAMETERSFILE}
  sample_rewards: !function src/environments/bandit/stationary/bernoulli.sample_rewards


pipeline: !GenericPipeline
  runs:
    - !TrialStage
      name: &trial_name fixed-noise-effect-thompson-sampling

      params:
        <<: *common_params
        trial: *trial_name
        bandit: *bernoulli_bandit
        algorithm: *thompson_sampling
        round_count: ${env.ROUNDCOUNT}

      inputs:
        src: !path
          path: src
        config: !path
          path: ${env.SOURCECODEFOLDER}/trial2.yml

      outputs:
        params: !path
          path: ${env.TARGETFOLDER}/params.json
        docstrings: !path
          path: ${env.TARGETFOLDER}/docstrings.json
        iteration_results: !path
          path: ${env.TARGETFOLDER}

      iteration_results:
        round: i
        actions: actions
        rewards: all_rewards
        mean_reward: mean_reward

      run: !python |
        params = algorithm.init_params(M)

        for i in range(int(round_count)):
            actions = algorithm.predict(l, params)
            all_rewards = bandit.sample_rewards(bandit.parametrization["parametrization"][i])
            params = algorithm.update(params, actions, all_rewards)
            mean_reward = sum(all_rewards[actions.tolist()])/len(actions.tolist())
            observe(iteration_results)
