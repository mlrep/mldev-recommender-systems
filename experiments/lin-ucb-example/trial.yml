# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md


common_params: &common_params
  M: &M 10
  l: &l 4
  d: &d 3


lin_ucb_algorithm: &lin_ucb_algorithm !Atom
  name: lin_ucb_algorithm
  alpha: 1
  <<: *common_params
  init_params: !function src/recommenders/bandit/lin_ucb.init_params
  predict: !function src/recommenders/bandit/lin_ucb.predict
  update: !function src/recommenders/bandit/lin_ucb.update


bernoulli_bandit: &bernoulli_bandit !Atom
  <<: *common_params
  parametrization: &parametrization [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 0.4]
  sample_rewards: !function src/environments/bandit/stationary/bernoulli.sample_rewards
  response: !python-function |
      def response(context, parametrization, M):
          reward = sample_rewards(parametrization)
          for action in range(M):
              context[action][0] += 1
              context[action][1] += reward[action]
              context[action][2] = context[action][1] / context[action][0]
          return reward, context
      return response


pipeline: !GenericPipeline
  runs:
    - !TrialStage
      name: lin-ucb-example
      description: "The Lin UCB algorithm example."

      params:
        <<: *common_params
        bandit: *bernoulli_bandit
        algorithm: *lin_ucb_algorithm
        round_count: ${env.ROUNDCOUNT}
        parametrization: *parametrization
        l: *l
        M: *M
        d: *d

      inputs:
        src: !path
          path: src
        config: !path
          path: experiments/lin-ucb-example/trial.yml

      outputs:
        params: !path
          path: ${env.TARGETFOLDER}/params.json
        docstrings: !path
          path: ${env.TARGETFOLDER}/docstrings.json
        iteration_results: !path
          path: ${env.TARGETFOLDER}

      iteration_results:
        round: i
        actions: actions
        reward: rewards
        parametrization: parametrization

      run: !python |
        import numpy as np
        
        
        alpha = algorithm.alpha
        a_matrices, b_vectors = algorithm.init_params(M, d)
        context = np.zeros(shape=(M, d))
    
        for i in range(int(round_count)):
            actions = algorithm.predict(context, a_matrices, b_vectors, alpha, l)
            rewards, context = bandit.response(context, parametrization, M)
            a_matrices, b_vectors = algorithm.update(a_matrices, b_vectors, context, rewards, actions)
            observe(iteration_results)
