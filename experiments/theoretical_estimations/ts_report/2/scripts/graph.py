import pandas as pd
import glob
import matplotlib.pyplot as plt
import numpy as np
import warnings
from math import log
import json
import ast

warnings.filterwarnings("ignore")

path = 'results/experiment_report_ts_theoretical_estimations/2/49/trial'

figure, axes = plt.subplots(1, 2, figsize=(12, 4))

ax = axes[1]

ks = [6, 6]
for j in range(2):
    for i in range(0, 30):
        params_path = f'{path}{j + 1}/trial-num:{i}/params.json'
        a = json.load(open(params_path))['bandit']['parametrization']['parametrization']
        a = [float(x) for x in ast.literal_eval(a)]

        k = ks[j]
        arms = sorted(a)
        regsum = sum(sorted(a)[-k:])
        a = np.array(a)

        path1 = f'{path}{j + 1}/trial-num:{i}'
        x = pd.read_parquet(glob.glob(path1 + '/data-*.parquet')[0])
        x['get_reward'] = pd.Series(map(lambda y: sum(a[y]), x['actions']), index=x.index)
        regret = np.cumsum(regsum - x['get_reward'].values)
        values = regret / list(map(lambda x: log(x + 1), np.arange(0, len(regret), 1)))

        limit = 0
        for i in range(len(arms) - k):
            q = arms[k - 1]
            p = arms[i + k]
            limit += (q - p) / (p * log(p / q) + (1 - p) * log((1 - p) / (1 - q)))

        ax = axes[j]
        ax.plot(values - limit)

plt.savefig('results/experiment_report_ts_theoretical_estimations/2/49/report_with_experiment_results2/figure_1.png')
print("it is done")
