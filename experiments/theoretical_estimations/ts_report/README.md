Эксперимент по проверке теоретических оценок для работы алгоритма Thompson Sampling.

lim Pr [\frac{Reg(T)}{log T} - \sum_{i \in K \ L}\frac{\Delta_{i, L}}{d(\mu_i, \mu_L)} = 0] = 1