import pandas as pd
import glob
import matplotlib.pyplot as plt
import numpy as np
import warnings
from math import log
import json

warnings.filterwarnings("ignore")

path = 'results/experiment_report_exp3_theoretical_estimations/2/49/trial'

figure, axes = plt.subplots(1, 2, figsize=(12, 4))

ax = axes[1]

g_max = None
gamma = 0.01
K = 10
g_exp3 = np.zeros(15010)
count = 30
for j in range(2):
    for i in range(0, count):
        sum_for_arms = np.zeros(K)
        path1 = f'{path}{j + 1}/trial-num:{i}'
        x = pd.read_parquet(glob.glob(path1 + '/data-*.parquet')[0])
        x['get_reward'] = pd.Series(map(lambda x, y: x[y][0], x['reward'], x['actions']), index=x.index)
        get_reward = np.cumsum(x['get_reward'].values)
        maximums = []
        for i in range(len(x)):
            sum_for_arms += x.iloc[i]['reward']
            maximums.append(max(sum_for_arms))  # выбор самой доходной ручки
        if g_max is None:
            g_max = maximums
        else:
            g_max = list(map(lambda x, y: max(x, y), g_max, maximums))
        g_exp3 += get_reward
    g_exp3 = np.array(g_exp3 / count)  # матожидание G_exp3
    g_max = np.array(g_max)
    ax = axes[j]
    ax.plot(g_max - g_exp3, color='black', label='weak regret')
    ax.plot(1.7 * gamma * g_max + K * log(K)/ gamma, color='green', label='upper bound')
    ax.legend()

plt.savefig('results/experiment_report_exp3_theoretical_estimations/2/49/report_with_experiment_results2/figure_1.png')
print("it is done")
