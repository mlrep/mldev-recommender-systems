import pandas as pd

import pickle
from collections import defaultdict
import os
import glob

DATA_PATH = os.environ['DATAPATH']
TARGET_PATH = DATA_PATH + '/experiment_data/'


for i, fold_path in enumerate(glob.glob(DATA_PATH + '/*')):
    fold_path += '/'

    with open(fold_path + 'train_users.pkl', 'rb') as f1, \
            open(fold_path + 'test_users.pkl', 'rb') as f2, \
            open(fold_path + 'item_features.pkl', 'rb') as f3:
        train_users = pickle.load(f1)
        test_users = pickle.load(f2)
        action_features = pickle.load(f3)

    train_df = pd.read_parquet(fold_path + 'train_dataset.parquet')
    test_df = pd.read_parquet(fold_path + 'test_dataset.parquet')

    train_df['rating'] /= 5
    test_df['rating'] /= 5

    train_user_to_favourite_actions = defaultdict(list)
    test_user_to_favourite_actions = defaultdict(list)

    for user in train_df.user_id.unique():
        chunk = train_df[(train_df['user_id'] == user) & (train_df['rating'] >= 0.8)]['item_id'].unique()
        train_user_to_favourite_actions[user].extend(chunk)

    for user in test_df.user_id.unique():
        chunk = test_df[(test_df['user_id'] == user) & (test_df['rating'] >= 0.8)]['item_id'].unique()
        test_user_to_favourite_actions[user].extend(chunk)

    parametrization = test_df.groupby('item_id')['rating'].sum().values

    if not os.path.exists(TARGET_PATH):
        os.mkdir(TARGET_PATH)

    train_df.to_parquet(TARGET_PATH + f'train_df_fold_{i}.parquet')
    with open(TARGET_PATH + f'train_user_to_favourite_actions_fold_{i}.pkl', 'wb') as f1, \
            open(TARGET_PATH + f'test_user_to_favourite_actions_fold_{i}.pkl', 'wb') as f2, \
            open(TARGET_PATH + f'action_features_fold_{i}.pkl', 'wb') as f3, \
            open(TARGET_PATH + f'parametrization_fold_{i}.pkl', 'wb') as f4:
        pickle.dump(train_user_to_favourite_actions, f1)
        pickle.dump(test_user_to_favourite_actions, f2)
        pickle.dump(action_features, f3)
        pickle.dump(parametrization, f4)
