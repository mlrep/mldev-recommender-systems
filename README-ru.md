# MLDev Recommender Systems

Экспериментальный стенд MLDev Recommender Systems позволяет исследователю выполнять воспроизводимые эксперименты
с подключением произвольного пользовательского кода, либо используя доступные в шаблоне алгоритмы рекомендательных
систем и модели поведения пользователей.

Воспроизводимость достигается за счет сбора и сохранения результатов и параметров проведенных экспериментов и
гарантируется при соблюдении
[определенных условий](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/Руководство-пользователя#условия-воспроизводимости-экспериментов).

# Приложения

- разработка алгоритмов рекомендаций;
- проведения научных исследований с рекомендательными системами;
- решения задач анализа данных, в том числе разработки алгоритмов машинного обучения, 
  и проведения вычислительных экспериментов с данными и алгоритмами

# Документация

Документация пользователя и разработчика доступна в [wiki](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis) проекта.

## Руководство пользователя

[Руководство пользователя](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/Руководство-пользователя) помогает:
- установить MLDev и инициализировать данный эксперементальный стенд
- запустить свой первый эксперимент
- изучить функции MLDev Recommender Systems

## Учебник

Учебник помогает решить типовые задачи и лучше освоить функциональность MLDev Recommender Systems.

Cценарии использования MLDev Recommender Systems приведены в [учебнике](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/%D0%A3%D1%87%D0%B5%D0%B1%D0%BD%D0%B8%D0%BA) в виде пошаговых руководств.

## Документация разработчика

Создание окружения для разработки, архитектура и рекомендации по разработке описаны в [документации разработчика](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/%D0%94%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D1%8F-%D1%80%D0%B0%D0%B7%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D1%87%D0%B8%D0%BA%D0%B0).

# Партнеры и поддержка

### Фонд Содействия Инновациям

<p>
<a href="https://fasie.ru/"><img src="https://fasie.ru/local/templates/.default/markup/img/logo_new.svg" alt="Фонд Содействия Инновациям" height="80px"/></a>
</p>

### Gitlab open source

<p>
<a href="https://about.gitlab.com/solutions/open-source/"><img src="https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png" alt="GitLab Open Source program" height="80px"></a>
</p> 


# Поддержка и контакты

Предоставить обратную связь, предложить функцию или сообщить о баге:
- Группа пользователей в [Телеграм](https://t.me/mldev_betatest)
- Канал [#mlrep](https://opendatascience.slack.com) в Slack OpenDataScience
- Трэкер задач [Gitlab](https://gitlab.com/mlrep/mldev-recommender-systems/-/issues)

# Участие

Чтобы участвовать в проекте, задать вопрос или предложить функцию, пожалуйста, ознакомьтесь с руководством [CONTRIBUTING.md](https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md). 

# Лицензия

Это программное обеспечение распространятся с лицензией [Apache 2.0 license](LICENSE).
