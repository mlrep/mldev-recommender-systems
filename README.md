#### Описание MLDev Recommender Systems на русском языке доступно по [ссылке](https://gitlab.com/mlrep/mldev-recommender-systems/-/blob/master/README-ru.md) и в русскоязычных статьях [wiki](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/Главная)


# MLDev Recommender Systems

The MLDev Recommender Systems experimental stand enables researchers to run reproducible experiments with
user code integration or with algorithms and user models available in the template.

Reproducibility is achieved by collecting and saving the results and parameters of experiments.
Reproducibility is guaranteed under
[the following conditions](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/Руководство-пользователя#условия-воспроизводимости-экспериментов). 

# Applications

- research and development of recommender systems algorithms and models
- scientific research and experimentation with recommender systems
- data mining problems solving, including development of machine learning algorithms and 
  execution of computational data experiments

# Documentation

User and development documentation is available in project [wiki](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/).

## User guide

The [user guide](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/User-Guide) helps with:
- MLDev installation and the Recommender Systems template initialization
- running the first experiment
- learning about MLDev Recommender Systems functions

## Tutorial

Tutorial helps with doing of practical tasks and to learn MLDev Recommender Systems capabilities better.

MLDev Recommender Systems use cases are provided in [tutorial](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/Tutorial) in for of step-by-step instructions.

## Developer documentation

To setup the developer environment, explore the MLDev Recommender Systems architecture, and start development, please follow [developer documentation](https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/Developer-documentation).

# Partners and supporters

### FASIE - Foundation for Assistance to Small Innovative Enterprises

<p>
<a href="https://fasie.ru/"><img src="https://www.fbras.ru/wp-content/uploads/2015/06/fasie_en__1_.png" alt="Foundation for Assistance to Small Innovative Enterprises" height="80px"/></a>
</p>

### Gitlab open source

<p>
<a href="https://about.gitlab.com/solutions/open-source/"><img src="https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png" alt="GitLab Open Source program" height="80px"></a>
</p> 


# Support and contacts

Give feedback, suggest feature, report bug:
- [Telegram](https://t.me/mldev_betatest) user group
- [#mlrep](https://opendatascience.slack.com) channel at OpenDataScience Slack
- [Gitlab](https://gitlab.com/mlrep/mldev-recommender-systems/-/issues) issue tracker

# Contributing

Please check the [CONTRIBUTING.md](https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md) guide if you'd like to participate in the project, ask a question or give a suggestion.

# License

The software is licensed under [Apache 2.0 license](LICENSE).
