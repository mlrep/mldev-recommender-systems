# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Flatten tag
===========

This module contains flatten tag.

* :py:class:`Flatten`
    Make a flat list of nested objects recursively.
"""

from typing import Iterator, Iterable

from mldev.experiment import experiment_tag


def _sequence_loader(cls):
    def wrapped(loader, node):
        node_seq = loader.construct_sequence(node, deep=True)
        return cls(node_seq)

    return wrapped


def _sequence_representer(cls):
    def wrapped(dumper, data):
        return dumper.represent_sequence(f"!{cls.__name__}", list(data))

    return wrapped


def _flatten(sequence: Iterable) -> Iterator:
    for el in sequence:
        if isinstance(el, Iterable):
            yield from _flatten(el)
        else:
            yield el


@experiment_tag(
    loader=_sequence_loader, representer=_sequence_representer, name="flatten"
)
class Flatten:
    """Make a flat list of nested objects recursively.

    This is a tag `!flatten`.

    Example of usage
    ----------------
    .. code-block::

        stages:
            - !BasicStage
                ...
            - !BasicStage
                ...

        pipeline: !GenericPipeline
          runs: !flatten
            - !BasicStage
                ...
            - *stages
            - !BasicStage
                ...
    """

    def __init__(self, sequence: Iterable):
        """Initialize `Flatten`.

        :param sequence: sequence to flatten
        """
        self.sequence = sequence

    def __iter__(self) -> Iterator:
        """Flatten recursively."""
        return _flatten(self.sequence)
