# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Atom tag
========
"""

from mldev.experiment import experiment_tag


@experiment_tag()
class Atom:
    """
    `Atom` is the smallest unit of experiment.

    `Atom` can be a part of another `Atom` or `TrialStage`.
    """

    def __init__(self, name=None, **kwargs):
        """Initialize `Atom`.

        :param name: an Atom's name
        """
        self.name = name
        self.attributes = {}
        self.callables = {}
        self.kwargs = kwargs

        for k, v in kwargs.items():
            if callable(v):
                self.callables[k] = v
            else:
                self.attributes[k] = v

        for k, v in self.attributes.items():
            self.add_attribute_to_callables(k, v)
        for k, v in self.callables.items():
            self.add_attribute_to_callables(k, v)

    def add_attribute_to_callables(self, name, attribute):
        """Add attribute to all Atom's callables (e.g. !function, !python)."""
        for callable_ in self.callables.values():
            if hasattr(callable_, '__globals__'):
                callable_.__globals__[name] = attribute

    def __deepcopy__(self, memo):
        return Atom(self.name, **self.kwargs)

    def __getattr__(self, attr):
        """Get attribute value from Atom's runtime context."""
        if attr in self.attributes:
            return self.attributes[attr]
        if attr in self.callables:
            return self.callables[attr]
        raise AttributeError(f"{attr} not found in the {self.name} atom's context")
