# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
BasicStageFactory tag
=====================

Contains the `mldev.experiment.BasicStages` factory.

* :py:class:`BasicStageFactory`
    The `mldev.experiment.BasicStages` factory.
"""

import copy

from experiment_reporting import Report, JsonQuery
from mldev.experiment import experiment_tag, BasicStage
from mldev.expression import Expression
from mldev.yaml_loader import stage_context

from stages.metrics.metric import MetricGroup
from metrics import *



@experiment_tag()
class BasicStageFactory:
    """The `mldev.experiment.BasicStages` factory.

    The factory creates `BasicStages` instances by substituting
    parameters with user values. It allows, for example, to iterate
    over a grid of parameters.

    Use it as a tag `!BasicStageFactory`.
    """

    def __init__(
        self,
        basename,
        random_seed,
        stages_output,
        iterable=None,
        params=None,
        env=None,
        inputs=None,
        outputs=None,
        script=None,
        trial_count=1,
    ):
        """Initialize `BasicStageFactory`.

        :param basename: `BasicStage.name`
        :param random_seed: is used for randomization of BasicStage instances behavior
        :param stages_output: files and folders that this stage produces
        :param iterable: iterable which can be used for `BasicStage` instance generation
        :param params: `BasicStage.params`
        :param env: `BasicStage.env`
        :param inputs: `BasicStage.inputs`
        :param outputs: `BasicStage.outputs`
        :param script: `BasicStage.script`
        :param trial_count: repeats the creation of instances trial_count times
        """
        self.basename = basename
        self.random_seed = random_seed
        self.stages_output = stages_output
        self.iterable = iterable
        self.params = params or {}
        self.env = env or {}
        self.inputs = inputs or {}
        self.outputs = outputs or {}
        self.script = script or []
        self.trial_count = trial_count

    def __call__(self, name, *args, experiment=None, **kwargs):
        """Execute `BasicStageFactory`.

        This method invoked by the pipeline. Params is not used.
        """
        with stage_context(self):
            iterable = self.iterable or [None]
            for current in iterable:
                current_random_seed_inc = 0
                for trial_number in range(self.trial_count):
                    self.params['trial_number'] = trial_number
                    if current:
                        self.params['iterable'] = copy.copy(self.iterable)
                    env = {k: str(v) for k, v in self.env.items()}
                    if current:
                        env.update(
                            {k.upper(): v for k, v in current.items()}
                        )

                    env['RANDOMSEED'] = int(self.random_seed) + current_random_seed_inc
                    current_random_seed_inc += 1

                    env['TRIALNUMBER'] = trial_number
                    if self.trial_count > 1:
                        env['RANDOMSEED'] += trial_number
                    stage = BasicStage(
                        self.basename,
                        self.params,
                        copy.copy(env),
                        self.inputs,
                        self.outputs,
                        [str(s) for s in self.script],
                    )
                    if isinstance(self.stages_output, list):
                        self.stages_output.append(stage)
                    elif isinstance(self.stages_output, dict):
                        self.stages_output[stage] = None


@experiment_tag()
class StageFactory:
    def __init__(
        self,
        tag,
        stages_output,
        iterable,
        params=None,
    ):
        self.tag = tag
        self.stages_output = stages_output
        self.iterable = iterable
        self.params = params or {}

    def __call__(self, name, *args, experiment=None, **kwargs):
        with stage_context(self):
            iterable = self.iterable or [None]
            for current in iterable:
                if current:
                    stage_params = {}
                    self.params['iterable'] = copy.copy(self.iterable)
                    for param in self.params:
                        if param != 'iterable':
                            stage_params[param] = self.params[param]
                            if issubclass(type(stage_params[param]), Expression):
                                stage_params[param] = str(stage_params[param])

                    cls = globals()[self.tag]
                    stage = cls(
                        **stage_params,
                    )
                    if hasattr(stage, 'prepare'):
                        stage.prepare('StageFactory')
                    if isinstance(self.stages_output, list):
                        self.stages_output.append(stage)
                    elif isinstance(self.stages_output, dict):
                        self.stages_output[stage] = None
