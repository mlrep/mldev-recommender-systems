# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
IterableGrid tag
================

This module contains IterableGrid tag.

* `IterableGrid`
    Generate the parameter grid from lists of parameters.
"""

from mldev.experiment import experiment_tag
from sklearn.model_selection import ParameterGrid


class AttrDict(dict):
    """Allow access to the dict's attributes with the dot (.) notation ``obj.attr``."""

    def __init__(self, *args, **kwargs):
        """Keep values as attributes.

        Extend `dict.__init__()` to keep values as attributes in `self.__dict__`.

        :param dict_: dict with values
        """
        super(AttrDict, self).__init__(*args, **kwargs)
        self.__dict__ = self


@experiment_tag(name="IterableGrid")
class IterableGrid:
    """Generate the parameter grid from lists of parameters."""

    def __init__(self, **kwargs):
        """Initialize `IterableGrid`.

        :param kwargs: dict with pairs param_name => list of values
        """
        self.grid = ParameterGrid(kwargs or {})

    def __iter__(self):
        """Use the object as an iterator."""
        self.current = None
        self.index = -1
        return self

    def __next__(self):
        """Change current state to the next value."""
        self.index += 1
        if self.index >= len(self.grid):
            self.current = None
            raise StopIteration
        else:
            self.current = AttrDict(self.grid[self.index])
            return self.current
