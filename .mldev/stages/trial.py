import copy
import inspect
import json
import os
import random

import numpy as np

from mldev.experiment import BasicStage, experiment_tag
from mldev.yaml_loader import stage_context

from stages.atom import Atom
from stages.executable import inject_final_call
from stages.serialize import serialize_trial_params, serialize_trial_docs
from stages.writers.parquet import ParquetWriter


class Observer:
    """The class collects and stores the measures."""

    def __init__(self, writer):
        """Initialize ``Observer``.

        :param writer: writer
        """
        self.writer = writer

        self._result = []
        self.was_used = False
        self.observe_number = 0

    @staticmethod
    def __get_item_from_frame(item, frame):
        try:
            item = str(item)
            return copy.deepcopy(
                eval(item, frame.f_back.f_locals)
            )
        except (NameError, AttributeError):
            pass

        try:
            item = str(item)
            return copy.deepcopy(
                eval(item, frame.f_back.f_locals)
            )
        except (NameError, AttributeError):
            pass

        raise AttributeError(f'Observed item {item} not found in the scope.')

    def observe(self, observed, caller_frame=None):
        """Add new observation.

        Add new observation with mapping runtime variables and
        measurement names ``measurement``.
        """
        if not self.was_used:
            self.was_used = True

        if caller_frame is None:
            caller_frame = inspect.currentframe()
        self.writer.add_row({
            k: self.__get_item_from_frame(v, caller_frame)
            for k, v in observed.items()
        })

        self.observe_number += 1

    def save(self):
        """Save all observations."""
        self.writer.write()


class ObserverRouter:
    def __init__(self):
        self.observers = {}

    def add_observer(self, writer, observer_dict):
        self.observers[id(observer_dict)] = Observer(writer)

    def route(self, observer_dict):
        caller_frame = inspect.currentframe()
        return self.observers[id(observer_dict)].observe(observer_dict, caller_frame)

    def save_results(self):
        for observer in self.observers.values():
            if observer and observer.was_used:
                observer.save()


@experiment_tag()
class TrialStage(BasicStage):
    def __init__(self, name, description=None, params=None, inputs=None,
                 outputs=None, init=None, run=None, **kwargs):
        super().__init__()
        self.name = name
        self.description = description
        self.params = params
        self.dict_inputs = inputs
        self.inputs = [v for k, v in inputs.items()]
        self.dict_outputs = outputs
        self.outputs = [v for k, v in outputs.items()]
        self.phases_callable = {
            'init': init,
            'run': run,
        }

        # add all params to each atom
        for atom_key, atom in self.params.items():
            if issubclass(type(atom), Atom):
                for param_key, param_val in self.params.items():
                    atom.add_attribute_to_callables(param_key, copy.deepcopy(param_val))

        # add params to callables
        for k, v in self.params.items():
            for phase in self.phases_callable:
                if self.phases_callable[phase]:
                    self.phases_callable[phase].__globals__[k] = copy.deepcopy(v)
        # add observers
        self.observer_router = ObserverRouter()
        for k, observer_dict in kwargs.items():
            path = str(self.dict_outputs[k])  # TODO: add stage_context?
            self.observer_router.add_observer(ParquetWriter(path), observer_dict)
            self.phases_callable['run'].__globals__[k] = observer_dict
        self.phases_callable['run'].__globals__['observe'] = self.observer_router.route

        # inject a final call for moving variables between init and run callables
        if self.phases_callable['init']:
            inject_final_call(self.phases_callable['init'], self._set_init_variables)

    def _set_init_variables(self, variables):
        for k, v in variables.items():
            self.phases_callable['run'].__globals__[k] = copy.deepcopy(v)

    def prepare(self, run_name=None):
        """Prepare ``Atom`` for execution.

        This method is called by the pipeline or another Atom.
        It has to initialize all dependencies, execute the ``init`` code,
        and return with no error if it is ready to be run, or it should raise
        an exception in other cases.
        """
        # Initialize the random seed.
        random_seed = os.environ.get('RANDOMSEED')
        if not random_seed:
            raise Exception('The random_seed parameter must be declared in the experiment.')
        random_seed = int(random_seed)
        np.random.seed(random_seed)
        random.seed(random_seed)

    def run(self, stage_name):
        """
        Called by the pipeline. Enter the ``stage_context`` and executes
        the callable object.

        :param stage_name: a name of the stage
        :return:
        """
        with stage_context(self) as stage:  # TODO: add stage to trial and atoms scopes?
            for phase in self.phases_callable:
                if self.phases_callable[phase]:
                    self.phases_callable[phase]()

            self.observer_router.save_results()

            if 'params' in self.dict_outputs:
                params_path = str(self.dict_outputs['params'])
                os.makedirs(os.path.dirname(params_path), exist_ok=True)
                with open(params_path, 'w') as f:
                    json.dump(self, f, default=serialize_trial_params)

            if 'docstrings' in self.dict_outputs:
                docs_path = str(self.dict_outputs['docstrings'])
                os.makedirs(os.path.dirname(docs_path), exist_ok=True)
                with open(docs_path, 'w') as f:
                    json.dump(self, f, default=serialize_trial_docs)
