import glob
from pathlib import Path
from typing import Dict, List, Tuple, Callable

import numpy as np
import pandas as pd

from mldev.experiment import experiment_tag


class Metric:
    """The basic class of metrics.

    Load and save the data. Calculation has to be implemented in a concrete metric class.
    """

    metric_name: str = None
    required_columns: tuple = None

    def __init_subclass__(cls, *args, **kwargs):
        for required in ('metric_name', 'required_columns',):
            if not getattr(cls, required):
                raise Exception(f"Can't instantiate class {cls.__name__}"
                                f"without `{required}` attribute defined")
        return super().__init_subclass__(*args, **kwargs)

    def __init__(
            self,
            data_path=None,
            result_path=None,
            columns_mapping=None,
            filter=None,
            append_results=False,
    ):
        self.data_path = data_path
        self.result_path = result_path
        self.columns_mapping = columns_mapping
        self.filter = filter
        self.append_results = append_results

    def __call__(self, *args, **kwargs):
        """Called by the pipeline. By default, ignore all parameters.

        Load source data, calculate metric and save the result data.
        """

        # str for ExpressionEval evaluating
        if self.data_path is not None:
            self.data_path = str(self.data_path)
        if self.result_path is not None:
            self.result_path = str(self.result_path)

        try:
            source_data_parts, source_data_files = self._load(
                self.data_path,
                self.required_columns,
                self.columns_mapping,
                self.filter,
            )
        except:
            return

        metric_data = self.calculate(source_data_parts)
        self._save(self.result_path, source_data_files, self.metric_name,
                   metric_data, self.append_results)

    def prepare(self, stage_name):
        """
        Called by the pipeline. By default, does nothing.

        :param stage_name: a name of the stage
        :return:
        """
        pass

    @staticmethod
    def calculate(source_data):
        raise Exception("Metric subclass should implement calculate method!")

    @staticmethod
    def _load(
            filepath_pattern: str,
            required_columns: Tuple,
            columns_mapping: List,
            filter: Callable,
    ):
        """Load data files by pattern.

        Return collection with as many items as there are folders.

        :param filepath_pattern: files pattern
        :param required_columns: required columns
        :param columns_mapping: columns mapping with transformations while loading data;
            if param is empty files loads as is
        :param filter: a function for filtering rows
        """

        data_parts = []
        source_data_files = glob.glob(filepath_pattern)
        for file in source_data_files:
            df = pd.read_parquet(file, engine='pyarrow')

            if df.empty:
                raise Exception('The source data is empty.')

            # apply filter to the source DataFrame
            if filter is not None:
                df = df[df.apply(filter, axis=1)]
            if df.empty:
                raise Exception('The source data is empty after filtration.')

            new_df = pd.DataFrame()
            for column in required_columns:
                if column in columns_mapping:
                    if callable(columns_mapping[column]):
                        transformation = columns_mapping[column]
                    else:
                        transformation = lambda row: columns_mapping[column]
                    new_df = new_df.assign(**{
                        column: df.apply(transformation, axis=1)
                    })
                elif column in df:
                    new_df[column] = df[column]
                else:
                    raise Exception(f"The expected column `{column}` "
                                    "is absent in the source data!")
            data_parts.append(new_df)
        return data_parts, source_data_files

    @staticmethod
    def _save_part(filepath: Path, data_part, append_results: bool):
        if append_results:
            prev_data = pd.read_parquet(filepath, engine='pyarrow')
            data_part = pd.concat([prev_data, data_part], axis=1)
            data_part = data_part.loc[:, ~data_part.columns.duplicated()].copy()
        data_part.to_parquet(filepath)

    @staticmethod
    def _save(
            filepath: str,
            source_data_files: List[str],
            metric_name: str,
            resultant_data_parts: np.array,
            append_results: bool,
    ):
        """Save each part of data to the appropriate folder."""

        if filepath:
            if len(resultant_data_parts) != 1:
                raise Exception("The resultant data parts count "
                                "and folders count are different.")
            path = Path(filepath)
            if path.is_dir():
                path = path.joinpath(f'{metric_name}.parquet')
            Metric._save_part(path, resultant_data_parts[0], append_results)
        else:
            if len(resultant_data_parts) != len(source_data_files):
                raise Exception("The resultant data parts count "
                                "and folders count are different.")
            for i in range(0, len(source_data_files)):
                path = Path(source_data_files[i]).parent
                filename = Path(source_data_files[i]).name
                assert path.is_dir()
                path = path.joinpath(f'{metric_name}_{filename}.parquet')
                Metric._save_part(path, resultant_data_parts[i], append_results)


@experiment_tag()
class MetricGroup:
    """This tag groups metric results in same files (file)."""

    def __init__(self, metrics, result_path, data_path=None, filter=None):
        """Calling `PythonExpression`.

        :param metrics: list of Metrics
        :param result_path: same as Metric's result_path
        :param data_path: same as Metric's data_path
        """
        self.metrics = metrics
        self.data_path = data_path
        self.result_path = result_path
        self.filter = filter

    def prepare(self, stage_name):
        """
        Called by the pipeline.
        Calls prepare method for every metric in `self.metrics` list and sets some params
        for grouping the result data.

        :param stage_name: a name of the stage
        :return:
        """
        is_first_metric_in_group = True
        for metric in self.metrics:
            metric.prepare(stage_name)

            if is_first_metric_in_group:
                metric.append_results = False
                is_first_metric_in_group = False
            else:
                metric.append_results = True
            metric.result_path = self.result_path
            if self.data_path and metric.data_path is None:
                metric.data_path = self.data_path
            if self.filter and metric.filter is None:
                metric.filter = self.filter

    def __call__(self, *args, **kwargs):
        """Called by the pipeline. By default, ignore all parameters.

        Calls every metric in `self.metrics` list.
        """
        for metric in self.metrics:
            metric(*args, **kwargs)
