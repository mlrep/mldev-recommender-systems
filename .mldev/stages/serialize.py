import ast
from pathlib import Path
import sys

from stages.atom import Atom


def __serialize_params(obj):
    """
    Serialize parameters recursively
    """
    def __serialize_atom(atom):
        dict_ = {}
        if hasattr(atom, 'name') and atom.name:
            dict_['name'] = atom.name
        for k, v in atom.attributes.items():
            dict_[k] = __serialize_params(v)
        if atom.callables:
            dict_['functions'] = {}
        for k, v in atom.callables.items():
            dict_['functions'][k] = __serialize_params(v)
        return dict_

    def __serialize_callable(callable_):
        if hasattr(callable_, '__source__'):
            res = {
                'language': 'python',
                'code': callable_.__source__,
            }
        else:
            function = f"{callable_.__module__.replace('.', '/')}.{callable_.__name__}"
            res = {'function': function}
        if callable_.__doc__:
            res['docstring'] = callable_.__doc__
        return res

    if isinstance(obj, dict):
        return {
            k: __serialize_params(v) for k, v in obj.items()
        }
    if isinstance(obj, Atom):
        return __serialize_atom(obj)
    if callable(obj):
        return __serialize_callable(obj)
    return str(obj)


def serialize_trial_params(trial_stage) -> dict:
    """
    Serialize TrialStage's parameters
    """
    trial_dict = __serialize_params(trial_stage.params)
    return __merge_dicts(
        trial_dict,
        __serialize_params(trial_stage.phases_callable)
    )


def __merge_dicts(dict1: dict, dict2: dict):
    """
    Recursively merge two dictionaries of dictionaries
    """
    if not isinstance(dict1, dict) or not isinstance(dict2, dict):
        raise ValueError('All attributes must be dicts.')

    merged_dict = dict1.copy()
    for key, value in dict2.items():
        if key in merged_dict:
            if not isinstance(merged_dict[key], dict) and not isinstance(value, dict):
                if merged_dict[key] != value:
                    raise ValueError(f'Different values while merging: '
                                     f'{merged_dict[key]} and {value}')
                # Doing nothing.
            else:
                merged_dict[key] = __merge_dicts(merged_dict[key], value)
        else:
            merged_dict[key] = value
    return merged_dict


def __serialize_docs(obj):
    def __serialize_atom(atom):
        atom_dict = {}
        for k, v in atom.callables.items():
            callable_dict = __serialize_callable(v)
            atom_dict = __merge_dicts(atom_dict, callable_dict)
        return atom_dict

    def __serialize_callable(callable_) -> dict:
        if not hasattr(callable_, '__module__') or callable_.__module__ is None:
            return {
                callable_.__name__: {
                    'type': 'function',
                    'docstring': callable_.__doc__,
                }
            }

        module_path = f"{callable_.__module__.replace('.', '/')}.py"

        file_path = Path(module_path).absolute()
        if not Path(module_path).is_file():
            for path in sys.path:
                if Path(path, module_path).is_file():
                    file_path = Path(path, module_path).absolute()
                    break

        with open(file_path) as fd:
            file_contents = fd.read()
        module = ast.parse(file_contents)
        module_dict = {'type': 'module'}
        docstring = ast.get_docstring(module)
        if docstring is not None:
            module_dict['docstring'] = docstring

        functions = [n for n in module.body if isinstance(n, ast.FunctionDef)]
        for function in functions:
            function_dict = {'type': 'function'}
            docstring = ast.get_docstring(function)
            if docstring is not None:
                function_dict['docstring'] = docstring
            module_dict[function.name] = function_dict

        classes = [n for n in module.body if isinstance(n, ast.ClassDef)]
        for class_ in classes:
            class_dict = {'type': 'class'}
            docstring = ast.get_docstring(class_)
            if docstring is not None:
                class_dict['docstring'] = docstring
            module_dict[class_.name] = class_dict

            methods = [n for n in class_.body if isinstance(n, ast.FunctionDef)]
            for method in methods:
                method_dict = {'type': 'method'}
                docstring = ast.get_docstring(method)
                if docstring is not None:
                    method_dict['docstring'] = docstring
                class_dict[method.name] = method_dict
        return {module_path: module_dict}

    if isinstance(obj, Atom):
        return __serialize_atom(obj)

    if callable(obj):
        return __serialize_callable(obj)

    return {}


def serialize_trial_docs(trial_stage) -> dict:
    """Serialize TrialStage's docstrings.

    Serialize all docstrings from functions, classes, and modules
    of the `TrialStage` recursively.
    """
    trial_dict = {}
    for k, v in trial_stage.params.items():
        param_dict = __serialize_docs(v)
        trial_dict = __merge_dicts(trial_dict, param_dict)

    for k, v in trial_stage.phases_callable.items():
        if v is not None:
            callable_dict = __serialize_docs(v)
            trial_dict = __merge_dicts(trial_dict, callable_dict)
    return trial_dict
