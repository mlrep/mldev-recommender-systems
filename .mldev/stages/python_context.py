# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Python context module
=====================

This module provides custom tags to import python functions directly into
the experiment.

When used in a experiment spec, the experiment should be loaded in ``EXPERIMENT`` environment.
No in the ``MLDEV`` environment as by default.

"""


from mldev.experiment_tag import _scalar_representer, _scalar_loader, experiment_tag


@experiment_tag(loader=_scalar_loader, representer=_scalar_representer, name="python-function")
class PythonFunctionFromCode:
    """
    Use this to load a specific python function into object graph from experiment.yaml

    This is a string scalar specifying a fully qualified name of the function
    """
    def __new__(cls, code: str, *args, **kwargs):
        if len(args) > 0 or len(kwargs) > 0:
            raise TypeError('!python-function accepts only one argument.')

        indentation = '    '
        indented_code = '\n'.join([indentation + line for line in code.split('\n')])
        code = 'def python_function_dummy():\n' + indented_code

        globals_dict = {}
        exec(code, globals_dict)
        python_function_dummy = globals_dict['python_function_dummy']
        func = python_function_dummy()
        if not callable(func):
            raise TypeError('Code in the !python-function must return a function.')
        func.__source__ = code

        return func
