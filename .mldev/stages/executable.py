# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Executable tags
===============

This module contains tags which can be executed.

* :py:class:s`PythonExecutable`
    This one contains the python code.
"""

import dis
import inspect
import itertools
import types
import uuid

from mldev.experiment import experiment_tag


def _scalar_representer(cls):
    def wrapped(dumper, data):
        return dumper.represent_scalar(f"!{cls.__name__}", str(data))

    return wrapped


def _scalar_loader(cls):
    def wrapped(loader, node):
        node_value = loader.construct_scalar(node)
        return cls(node_value)

    return wrapped


@experiment_tag(name='python', loader=_scalar_loader, representer=_scalar_representer)
class PythonCallable:
    """This is a tag that contains executable python code."""

    def __new__(cls, code_str, *args, **kwargs):
        code_obj = compile(code_str, '<string>', 'exec')
        unique_name = 'python_callable_' + str(uuid.uuid4()).replace('-', '')
        f = types.FunctionType(code_obj, {'__builtins__': __builtins__}, unique_name)
        f.__source__ = code_str
        return f


# Inject a final call for getting Callable's local context.
def inject_final_call(fn, variable_setter):
    def get_callable_local_context(setter, injected_object_name):
        def f():
            local_vars = {}
            caller_frame = inspect.currentframe()
            for k, v in caller_frame.f_back.f_locals.items():
                if k not in ('__builtins__', injected_object_name):
                    local_vars[k] = v
            setter(local_vars)

        return f

    uniq_obj_name = str(uuid.uuid4())
    fn.__globals__[uniq_obj_name] = get_callable_local_context(
        variable_setter, uniq_obj_name)

    index = len(fn.__code__.co_names)
    new_names = fn.__code__.co_names + (uniq_obj_name, )
    instructions = [
        ('LOAD_GLOBAL', index),
        ('CALL_FUNCTION', 0),
        ('POP_TOP', 0),
    ]
    call_code = bytes(itertools.chain(*[(dis.opmap[instruction], argument)
                                        for instruction, argument in instructions]))
    new_instructions = fn.__code__.co_code[:-4] + call_code + fn.__code__.co_code[-4:]

    # This conditional handles differences in the types.CodeType constructor
    # based on Python version. It can be removed once Python 3.8+ is the
    # minimum supported version, simplifying the code and leveraging
    # methods like `types.CodeType.replace` for code object manipulation.
    if hasattr(types.CodeType, "co_posonlyargcount"):
        new_code = types.CodeType(
            fn.__code__.co_argcount,
            fn.__code__.co_posonlyargcount,
            fn.__code__.co_kwonlyargcount,
            fn.__code__.co_nlocals,
            fn.__code__.co_stacksize,
            fn.__code__.co_flags,
            new_instructions,
            fn.__code__.co_consts,
            new_names,
            fn.__code__.co_varnames,
            fn.__code__.co_filename,
            fn.__code__.co_name,
            fn.__code__.co_firstlineno,
            fn.__code__.co_lnotab,
            fn.__code__.co_freevars,
            fn.__code__.co_cellvars
        )
    else:
        new_code = types.CodeType(
            fn.__code__.co_argcount,
            fn.__code__.co_kwonlyargcount,
            fn.__code__.co_nlocals,
            fn.__code__.co_stacksize,
            fn.__code__.co_flags,
            new_instructions,
            fn.__code__.co_consts,
            new_names,
            fn.__code__.co_varnames,
            fn.__code__.co_filename,
            fn.__code__.co_name,
            fn.__code__.co_firstlineno,
            fn.__code__.co_lnotab,
            fn.__code__.co_freevars,
            fn.__code__.co_cellvars
        )
    fn.__code__ = new_code
