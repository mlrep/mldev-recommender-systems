# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Loggers
=======

Logging module for mldev-recommenderer-system.

To use, simply 'import logger'.
"""

import os
from pathlib import Path

LOGGER_ENV_VARIABLE_NAME = 'MLDEV_RECSYS_LOGGER'


def _create(logger_type):
    logger_type = logger_type.lower()
    if logger_type == 'tensorboard':
        from stages.loggers.tensorboard import TensorBoardLogger

        logs_path = Path('logs', os.environ.get('LOGPATH'))
        os.makedirs(logs_path, exist_ok=True)
        return TensorBoardLogger(str(logs_path))
    if logger_type == 'parquet':
        from stages.loggers.parquet import ParquetLogger

        return ParquetLogger()
    if logger_type == 'sqlite':
        from stages.loggers.sqlite import SQLiteLogger

        return SQLiteLogger()
    raise Exception(f'Unknown logger type {logger_type}')


logger = None
if LOGGER_ENV_VARIABLE_NAME in os.environ:
    logger = _create(os.environ[LOGGER_ENV_VARIABLE_NAME])
