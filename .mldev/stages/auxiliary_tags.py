# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
Auxiliary tags
===============

This module contains auxiliary tags.

* :py:class:s`PythonExpression`
    This one contains the python expression.
"""

import numpy
import scipy

from mldev.experiment import experiment_tag


def _scalar_representer(cls):
    def wrapped(dumper, data):
        return dumper.represent_scalar(f"!{cls.__name__}", str(data))

    return wrapped


def _scalar_loader(cls):
    def wrapped(loader, node):
        node_value = loader.construct_scalar(node)
        return cls(node_value)

    return wrapped


@experiment_tag(loader=_scalar_loader, representer=_scalar_representer)
class PythonExpression:
    """This is a tag that evaluate python code and contains a result."""

    def __new__(cls, expression):
        """Calling `PythonExpression`.

        :param expression: python expression
        """
        return eval(expression)
