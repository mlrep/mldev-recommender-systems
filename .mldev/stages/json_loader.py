# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""
JsonLoader tag
==============

This module contains JsonLoader tag.

* :py:class:`JsonLoader`
    Load JSON object from file.
"""

import json

from mldev.experiment import experiment_tag
from mldev.expression import Expression, PARAM_PATTERN
from mldev.yaml_loader import stage_context


def _json_loader(cls):
    def wrapped(loader, node):
        filepath = loader.construct_scalar(node)
        is_expression = PARAM_PATTERN.findall(filepath)
        if is_expression:
            with stage_context(node):
                filepath = str(Expression(filepath))
        with open(filepath, 'r') as fp:
            return json.load(fp)

    return wrapped


def _dummy_representer(cls):
    return None


@experiment_tag(
    loader=_json_loader, representer=_dummy_representer
)
class JsonLoader:
    """JsonLoader tag.

    Load JSON object from file.
    """

    # There is no need to do anything because all the work is done by the loader.
    ...
