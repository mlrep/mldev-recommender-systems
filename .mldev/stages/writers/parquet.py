import os
from pathlib import Path

import pandas as pd


class AbstractWriter:
    ...


class ParquetWriter(AbstractWriter):
    """The writer class for parquet format.

    This one stores measures and saves them to the file.

    Use :py:meth:`ParquetWriter.add_row` to add new data to
    :py:attr:`ParquetWriter.result`.
    Use :py:meth:`ParquetWriter.write` to save this data in files
    `path/data-*.parquet`, where `path` is :py:attr:`ParquetWriter.path`.
    """

    def __init__(self, path):
        """Initialize `ParquetWriter`.

        :param path: path for storing parquet files
        """
        self.path = path
        self.result = []
        self.chunk_num = 0

    def write(self):
        """Save the `ParquetWriter.result` data in files."""
        if len(self.result) == 0:
            return

        os.makedirs(self.path, exist_ok=True)
        os.utime(self.path, None)
        file_path = Path(self.path, f'data-{self.chunk_num}.parquet')

        df = pd.DataFrame(self.result)
        df.to_parquet(file_path)

    def add_row(self, row):
        """Add new data to `ParquetWriter.result`."""
        self.result.append(row)
