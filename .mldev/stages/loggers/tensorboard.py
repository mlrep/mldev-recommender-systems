# Licensed under the Apache License: http://www.apache.org/licenses/LICENSE-2.0
# For details: https://gitlab.com/mlrep/mldev/-/blob/master/NOTICE.md

"""The module contains logger for TensorBoard."""

import tensorflow as tf


class TensorBoardLogger:
    """This is a logger for TensorBoard."""

    def __init__(self, log_dir):
        """Create file writer for TensorBoard summary."""
        self.writer = tf.summary.create_file_writer(log_dir)

    def scalar(self, tag, value, step):
        """Log scalar value."""
        with self.writer.as_default():
            tf.summary.scalar(tag, value, step=step)
        self.writer.flush()
