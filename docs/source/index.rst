.. mldev-recommender-systems documentation master file

MLDev Recommender Systems
=========================

.. toctree::
   :hidden:

   self

This is the official documentation for the MLDev Recommender Systems software.
MLDev Recommender System is a pre-built MLDev template to develop and experiment with recommender systems.

It is based on idea of experimental stand, which allows to put algorithms and user models on it,
and measure their interaction. MLDev Recommender System includes already implemented algorithms, user models
and metrics. But also can be extended by researcher in an agile way.

MLDev Recommender System uses MLDev core to provide experiment reproducibility.

.. toctree::
   :maxdepth: 1
   :caption: User documentation

   mldev-rec-sys.user-guide
   mldev-rec-sys.tutorial

.. toctree::
   :maxdepth: 1
   :caption: API Reference

   mldev-rec-sys.api_reference

See more at the project `Wiki <https://gitlab.com/mlrep/mldev-recommender-systems/-/wikis/home>`_

Partners and supporters
-----------------------

**FASIE - Foundation for Assistance to Small Innovative Enterprises**

.. image:: https://www.fbras.ru/wp-content/uploads/2015/06/fasie_en__1_.png
   :alt: Foundation for Assistance to Small Innovative Enterprises
   :height: 80px
   :target: https://fasie.ru/

**Gitlab open source**

.. image:: https://gitlab.com/mlrep/mldev/-/wikis/images/gitlab-logo-gray-stacked-rgb.png
   :alt: GitLab Open Source program
   :height: 80px
   :target: https://about.gitlab.com/solutions/open-source/

Support and contacts
--------------------

Give feedback, suggest feature, report bug:

* `Telegram <https://t.me/mldev_betatest>`_ user group
* `#mlrep <https://opendatascience.slack.com>`_ channel at OpenDataScience Slack
* `Gitlab <https://gitlab.com/mlrep/mldev-recommender-systems/-/issues>`_ issue tracker

Contributing
------------

Please check the `CONTRIBUTING.md <https://gitlab.com/mlrep/mldev/-/blob/develop/CONTRIBUTING.md>`_
guide if you'd like to participate in the project, ask a question or give a suggestion.

License
-------

The software is licensed under `Apache 2.0 license <https://gitlab.com/mlrep/mldev/-/blob/develop/LICENSE>`_

Index and tables
----------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
