Bandit
======

.. toctree::
   :maxdepth: 1

   bandit/mldev-rec-sys.base

.. toctree::
   :maxdepth: 2

   bandit/mldev-rec-sys.adversarial
   bandit/mldev-rec-sys.drift
   bandit/mldev-rec-sys.stationary
