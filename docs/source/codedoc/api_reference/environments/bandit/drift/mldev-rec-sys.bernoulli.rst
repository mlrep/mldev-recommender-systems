Bernoulli
---------

.. automodule:: environments.bandit.drift.bernoulli
   :members:
   :show-inheritance:
