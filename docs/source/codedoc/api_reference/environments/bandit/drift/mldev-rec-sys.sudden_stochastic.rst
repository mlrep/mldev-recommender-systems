Sudden Stochastic
-----------------

.. automodule:: environments.bandit.drift.sudden_stochastic
   :members:
   :show-inheritance:
