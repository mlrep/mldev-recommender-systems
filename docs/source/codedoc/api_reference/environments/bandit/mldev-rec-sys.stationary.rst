Stationary
==========

.. toctree::
   :maxdepth: 1

   stationary/mldev-rec-sys.bernoulli
   stationary/mldev-rec-sys.gauss
