Bernoulli Bandit
----------------

.. automodule:: environments.bandit.stationary.bernoulli
   :members:
   :show-inheritance:
