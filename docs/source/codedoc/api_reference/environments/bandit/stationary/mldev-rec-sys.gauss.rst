Gauss Bandit
------------

.. automodule:: environments.bandit.stationary.gauss
   :members:
   :show-inheritance:
