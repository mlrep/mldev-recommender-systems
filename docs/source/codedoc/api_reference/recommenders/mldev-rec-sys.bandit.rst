Bandit
======

.. toctree::
   :maxdepth: 1

   bandit/mldev-rec-sys.c2ucb
   bandit/mldev-rec-sys.e_greedy
   bandit/mldev-rec-sys.exp3
   bandit/mldev-rec-sys.exp3_m
   bandit/mldev-rec-sys.f-dsw-noisy-ts
   bandit/mldev-rec-sys.f-dsw-ts
   bandit/mldev-rec-sys.lin_ucb
   bandit/mldev-rec-sys.optimal
   bandit/mldev-rec-sys.random
   bandit/mldev-rec-sys.thompson_sampling
