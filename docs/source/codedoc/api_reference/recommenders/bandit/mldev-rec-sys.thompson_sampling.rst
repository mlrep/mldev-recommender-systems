Thompson Sampling
-----------------

.. automodule:: recommenders.bandit.thompson_sampling
   :members:
   :show-inheritance:
