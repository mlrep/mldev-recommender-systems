f-dsw Thompson Sampling with noise
----------------------------------

.. automodule:: recommenders.bandit.f_dsw_noisy_ts
   :members:
   :show-inheritance:
