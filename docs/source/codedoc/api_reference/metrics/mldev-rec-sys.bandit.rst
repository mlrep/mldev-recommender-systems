Bandit
======

.. toctree::
   :maxdepth: 1

   bandit/mldev-rec-sys.actions_probability
   bandit/mldev-rec-sys.cumulative_drift_trajectory
   bandit/mldev-rec-sys.cumulative_oracle_action_percentage
   bandit/mldev-rec-sys.diversity
   bandit/mldev-rec-sys.expected_adversarial_regret
   bandit/mldev-rec-sys.expected_stochastic_regret
   bandit/mldev-rec-sys.novelty
   bandit/mldev-rec-sys.oracle_action_probability
   bandit/mldev-rec-sys.precision
