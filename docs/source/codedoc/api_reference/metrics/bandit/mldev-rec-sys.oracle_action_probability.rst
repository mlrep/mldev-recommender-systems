Oracle Action Probability
-------------------------

.. automodule:: metrics.bandit.oracle_action_probability
   :members:
   :show-inheritance:
