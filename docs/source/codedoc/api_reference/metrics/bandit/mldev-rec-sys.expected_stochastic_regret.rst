Expected Stochastic Regret
--------------------------

.. automodule:: metrics.bandit.expected_stochastic_regret
   :members:
   :show-inheritance:
