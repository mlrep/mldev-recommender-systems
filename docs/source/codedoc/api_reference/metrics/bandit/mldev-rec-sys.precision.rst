Precision
---------

.. automodule:: metrics.bandit.precision
   :members:
   :show-inheritance:
