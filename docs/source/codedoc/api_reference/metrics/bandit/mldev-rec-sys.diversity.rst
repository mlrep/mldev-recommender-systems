Diversity
---------

.. automodule:: metrics.bandit.diversity
   :members:
   :show-inheritance:
