Cumulative Oracle Action Percentage
-----------------------------------

.. automodule:: metrics.bandit.cumulative_oracle_action_percentage
   :members:
   :show-inheritance:
