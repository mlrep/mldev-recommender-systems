Expected Adversarial Regret
---------------------------

.. automodule:: metrics.bandit.expected_adversarial_regret
   :members:
   :show-inheritance:
