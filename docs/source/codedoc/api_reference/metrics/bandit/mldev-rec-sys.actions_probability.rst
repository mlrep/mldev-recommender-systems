Actions Probability
-------------------

.. automodule:: metrics.bandit.actions_probability
   :members:
   :show-inheritance:
