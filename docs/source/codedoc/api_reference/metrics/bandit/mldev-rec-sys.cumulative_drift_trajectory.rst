Drift Divergence
----------------

.. automodule:: metrics.bandit.cumulative_drift_trajectory
   :members:
   :show-inheritance:
