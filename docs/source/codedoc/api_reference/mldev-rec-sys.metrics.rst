Metrics
-------

.. toctree::
   :maxdepth: 1

   metrics/mldev-rec-sys.average_by_trial_measurement

.. toctree::
   :maxdepth: 2

   metrics/mldev-rec-sys.bandit
