Base Template
-------------

.. toctree::
   :maxdepth: 1

   base_template/mldev-rec-sys.atom
   base_template/mldev-rec-sys.auxiliary_tags
   base_template/mldev-rec-sys.basic_stage_factory
   base_template/mldev-rec-sys.executable
   base_template/mldev-rec-sys.flatten
   base_template/mldev-rec-sys.iterable_grid
   base_template/mldev-rec-sys.json_loader
   base_template/mldev-rec-sys.logger
   base_template/mldev-rec-sys.python_context
   base_template/mldev-rec-sys.serialize
   base_template/mldev-rec-sys.trial
