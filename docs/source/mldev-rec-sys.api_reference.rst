API Reference
-------------

.. toctree::
   :maxdepth: 2

   codedoc/api_reference/mldev-rec-sys.base_template
   codedoc/api_reference/mldev-rec-sys.recommenders
   codedoc/api_reference/mldev-rec-sys.environments
   codedoc/api_reference/mldev-rec-sys.metrics
